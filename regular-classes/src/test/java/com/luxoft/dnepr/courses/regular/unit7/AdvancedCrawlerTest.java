package com.luxoft.dnepr.courses.regular.unit7;

import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class AdvancedCrawlerTest {
    @Test
    public void testExecute() {
        String rootDirPath = System.getProperty("user.home");
        FileCrawler crawlerMultiplyThreads = new FileCrawler(rootDirPath, 10);
        FileCrawlerResults resultsMultiplyThreads = crawlerMultiplyThreads.execute();

        FileCrawler crawlerTwoThreads = new FileCrawler(rootDirPath, 2);
        FileCrawlerResults resultsTwoThreads = crawlerTwoThreads.execute();

        assertEquals(resultsMultiplyThreads.getWordStatistics().size(), resultsTwoThreads.getWordStatistics().size());
    }
}
