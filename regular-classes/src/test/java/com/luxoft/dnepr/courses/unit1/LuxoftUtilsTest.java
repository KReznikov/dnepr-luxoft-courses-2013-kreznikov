package com.luxoft.dnepr.courses.unit1;

import org.junit.Assert;
import org.junit.Test;

public class LuxoftUtilsTest {

    @Test
    public void testGetMonthName() {
        Assert.assertEquals("January", LuxoftUtils.getMonthName(1, "en"));
        Assert.assertEquals("Март", LuxoftUtils.getMonthName(3, "ru"));
        Assert.assertEquals("Unknown Month", LuxoftUtils.getMonthName(31, "en"));
        Assert.assertEquals("Unknown Language", LuxoftUtils.getMonthName(7, "kz"));
        Assert.assertEquals("Unknown Language", LuxoftUtils.getMonthName(5, ""));
        Assert.assertEquals("Unknown Language", LuxoftUtils.getMonthName(12, null));
    }

    @Test
    public void testBinaryToDecimal() {
        Assert.assertEquals("91", LuxoftUtils.binaryToDecimal("1011011"));
        Assert.assertEquals("49", LuxoftUtils.binaryToDecimal("110001"));
        Assert.assertEquals("Not binary", LuxoftUtils.binaryToDecimal("-110001"));
        Assert.assertEquals("Not binary", LuxoftUtils.binaryToDecimal("120701"));
        Assert.assertEquals("Not binary", LuxoftUtils.binaryToDecimal("1b"));
        Assert.assertEquals("Not binary", LuxoftUtils.binaryToDecimal(""));
        Assert.assertEquals("Not binary", LuxoftUtils.binaryToDecimal(null));
    }

    @Test
    public void testDecimalToBinary() {
        Assert.assertEquals("11001", LuxoftUtils.decimalToBinary("25"));
        Assert.assertEquals("10011", LuxoftUtils.decimalToBinary("19"));
        Assert.assertEquals("110001", LuxoftUtils.decimalToBinary("49"));
        Assert.assertEquals("Not decimal", LuxoftUtils.decimalToBinary("-82"));
        Assert.assertEquals("Not decimal", LuxoftUtils.decimalToBinary("2a"));
        Assert.assertEquals("Not decimal", LuxoftUtils.decimalToBinary(""));
        Assert.assertEquals("Not decimal", LuxoftUtils.decimalToBinary(null));
    }


    @Test
    public void testSortArray() {
        int[] inputArray = {3, 6, 5, 11, -1, 8};
        int[] expectedArrayTrue = {-1, 3, 5, 6, 8, 11};
        int[] expectedArrayFalse = {11, 8, 6, 5, 3, -1};
        Assert.assertArrayEquals(expectedArrayTrue, LuxoftUtils.sortArray(inputArray, true));
        Assert.assertArrayEquals(expectedArrayFalse, LuxoftUtils.sortArray(inputArray, false));
    }

}
