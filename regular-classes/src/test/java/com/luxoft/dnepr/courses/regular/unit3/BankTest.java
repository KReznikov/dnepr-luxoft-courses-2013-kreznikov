package com.luxoft.dnepr.courses.regular.unit3;


import com.luxoft.dnepr.courses.regular.unit3.exceptions.FieldFormatException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.NoUserFoundException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.TransactionException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletDoesNotExistException;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class BankTest {
    @Test
    public void baseTest() throws TransactionException, NoUserFoundException, FieldFormatException,
            WalletDoesNotExistException {
        Bank bank = new Bank(System.getProperty("java.version"));
        Map<Long, UserInterface> users = new HashMap<>();

        Wallet johnsWallet = new Wallet(12L, new BigDecimal("10.00"), new BigDecimal("99.99"), WalletStatus.ACTIVE);
        User john = new User(3457585900L, "John", johnsWallet);
        Wallet marksWallet = new Wallet(12L, new BigDecimal("10.00"), new BigDecimal("99.99"), WalletStatus.ACTIVE);
        User mark = new User(3663264920L, "Mark", marksWallet);

        users.put(john.getId(), john);
        users.put(mark.getId(), mark);
        bank.setUsers(users);

        bank.makeMoneyTransaction(mark.getId(), john.getId(), new BigDecimal("5.00"));
    }


    @Test
    public void cashProblemsAndUserNotFoundTest() throws NoUserFoundException, TransactionException,
            WalletDoesNotExistException, FieldFormatException {

        Bank bank = new Bank(System.getProperty("java.version"));
        Map<Long, UserInterface> users = new HashMap<>();

        Wallet harrysWallet = new Wallet(1286544L, new BigDecimal("15"), new BigDecimal("20.99"), WalletStatus.ACTIVE);
        User harry = new User(3606464920L, "Harry", harrysWallet);
        users.put(harry.getId(), harry);

        Wallet johnsWallet = new Wallet(1442L, new BigDecimal("0"), new BigDecimal("10.99"), WalletStatus.ACTIVE);
        User john = new User(3376464920L, "John", johnsWallet);
        users.put(john.getId(), john);
        bank.setUsers(users);

        try {
            bank.makeMoneyTransaction(harry.getId(), 58965958L, new BigDecimal("2.99"));
        } catch (NoUserFoundException e) {
            Assert.assertEquals(e.getMessage(), "User with id 58965958 not found");
        }

        try {
            bank.makeMoneyTransaction(john.getId(), harry.getId(), new BigDecimal("2.99"));
        } catch (TransactionException e) {
            Assert.assertEquals(e.getMessage(), "User 'John' has insufficient funds (0.00 < 2.99)");
        }

        try {
            bank.makeMoneyTransaction(harry.getId(), john.getId(), new BigDecimal("11.00"));
        } catch (TransactionException e) {
            Assert.assertEquals(e.getMessage(), "User 'John' wallet limit exceeded (0.00 + 11.00 > 10.99)");
        }

    }

    @Test
    public void blockedWalletTest() throws NoUserFoundException, TransactionException,
            WalletDoesNotExistException, FieldFormatException {

        Bank bank = new Bank(System.getProperty("java.version"));
        Map<Long, UserInterface> users = new HashMap<>();

        Wallet harrysWallet = new Wallet(1286544L, new BigDecimal("15"), new BigDecimal("20.99"), WalletStatus.BLOCKED);
        User harry = new User(3606464920L, "Harry", harrysWallet);
        users.put(harry.getId(), harry);

        Wallet johnsWallet = new Wallet(1442L, new BigDecimal("0"), new BigDecimal("10.99"), WalletStatus.BLOCKED);
        User john = new User(3376464920L, "John", johnsWallet);
        users.put(john.getId(), john);
        bank.setUsers(users);


        try {
            bank.makeMoneyTransaction(harry.getId(), john.getId(), new BigDecimal("11.00"));
        } catch (TransactionException e) {
            Assert.assertEquals(e.getMessage(), "User 'Harry' wallet is blocked");
        }
        harrysWallet.setStatus(WalletStatus.ACTIVE);

        try {
            bank.makeMoneyTransaction(harry.getId(), john.getId(), new BigDecimal("11.00"));
        } catch (TransactionException e) {
            Assert.assertEquals(e.getMessage(), "User 'John' wallet is blocked");
        }
        johnsWallet.setStatus(WalletStatus.ACTIVE);

        bank.makeMoneyTransaction(harry.getId(), john.getId(), new BigDecimal("5.1"));
    }

}
