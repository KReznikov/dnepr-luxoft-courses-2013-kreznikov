package com.luxoft.dnepr.courses.regular.unit2;

import org.junit.Test;

import java.util.Date;
import java.util.GregorianCalendar;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;


public class ProductsTest {
    private ProductFactory productFactory = new ProductFactory();

    @Test
    public void testClone() throws Exception {
        Book book = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book cloned = book.clone();
        cloned.getPublicationDate().setTime(12323);
        assertNotEquals(book,cloned);

        Bread bread = productFactory.createBread("any code", "Krasnodaskiy", 20, 0.9);
        Bread breadCloned = (Bread) bread.clone();
        assertEquals(bread,breadCloned);

        Beverage beverage = productFactory.createBeverage("oh_man", "Mirgorodskya", 20, true);
        Beverage beverageCloned = (Beverage)  beverage.clone();
        assertEquals(beverage,beverageCloned);
        beverageCloned.setName("Borjomi");
        assertNotEquals(beverage,beverageCloned);

        Book book2 = productFactory.createBook("code", "Thinking in Java", 200, null);
        Book cloned2 = book2.clone();
        assertEquals(book2,cloned2);


    }

    @Test
    public void testEquals() throws Exception {
        Date date = new GregorianCalendar(2006, 0, 1).getTime();
        Book book = productFactory.createBook("code", "Thinking in Java", 200, date);
        Bread book2 = productFactory.createBread("code", "Thinking in Java", 200,0.5);
        assertNotEquals(book,book2);

        Book book3 = productFactory.createBook("code", "Thinking in Java", 220, date);
        assertEquals(book, book3);

        Book book4 = productFactory.createBook(null, "Thinking in Java", 220, date);
        assertNotEquals(book4, book);

        Book book5 = null;
        assertNotEquals(book5, book);

        Book book6 = null;
        assertEquals(book5, book6);
    }
}
