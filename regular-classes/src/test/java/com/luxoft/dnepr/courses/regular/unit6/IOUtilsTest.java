package com.luxoft.dnepr.courses.regular.unit6;


import com.luxoft.dnepr.courses.regular.unit6.familytree.FamilyTree;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Gender;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Person;
import com.luxoft.dnepr.courses.regular.unit6.familytree.impl.FamilyTreeImpl;
import com.luxoft.dnepr.courses.regular.unit6.familytree.impl.PersonImpl;
import com.luxoft.dnepr.courses.regular.unit6.familytree.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.*;

public class IOUtilsTest {
    @Test
    public void baseTest() throws IOException, ClassNotFoundException {
        Person joe = new PersonImpl(39, null, null, null, null, null);
        Person monika = new PersonImpl(32, "Monika", "en", Gender.FEMALE, null, null);
        Person brad = new PersonImpl(62, "Brad", "en", Gender.MALE, monika, null);
        Person anna = new PersonImpl(26, "Anna", "en", Gender.FEMALE, monika, joe);
        Person mark = new PersonImpl(24, "Mark", "en", Gender.MALE, anna, brad);
        Person sara = new PersonImpl(52, "Sara", "en", Gender.FEMALE, anna, brad);
        FamilyTree ft = FamilyTreeImpl.create(new PersonImpl(97, "Harry", "ua", Gender.MALE, sara, mark));
        IOUtils.save(System.getProperty("user.home") + "\\\\file.json", ft);

        FamilyTree ftDeserialized = IOUtils.load(System.getProperty("user.home") + "\\\\file.json");
        Assert.assertEquals(ft.getRoot(), ftDeserialized.getRoot());


        FamilyTree ftNull = FamilyTreeImpl.create(null);
        IOUtils.save("D:\\null.json", ftNull);
        IOUtils.load("D:\\null.json");

        PipedOutputStream pos = new PipedOutputStream();
        PipedInputStream pis = new PipedInputStream(pos);
        IOUtils.save(pos, ft);
        IOUtils.load(pis);
    }

}
