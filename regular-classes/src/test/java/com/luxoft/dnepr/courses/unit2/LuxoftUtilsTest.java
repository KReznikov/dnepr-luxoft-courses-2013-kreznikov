package com.luxoft.dnepr.courses.unit2;

import com.luxoft.dnepr.courses.unit2.model.*;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class LuxoftUtilsTest {

    @Test
    public void testSortArray() {
        String[] testArray = {"b", "d", "c", "a", "e", "Z", "A"};
        String[] expectedArrayTrue = {"A", "Z", "a", "b", "c", "d", "e"};
        String[] expectedArrayFalse = {"e", "d", "c", "b", "a", "Z", "A"};
        Assert.assertArrayEquals(expectedArrayTrue, LuxoftUtils.sortArray(testArray, true));
        Assert.assertArrayEquals(expectedArrayFalse, LuxoftUtils.sortArray(testArray, false));
        Assert.assertArrayEquals(new String[]{"a"}, LuxoftUtils.sortArray(new String[]{"a"}, false));

    }

    @Test
    public void testWordAverageLength() {
        Assert.assertEquals(3.25, LuxoftUtils.wordAverageLength("my name is javac"), 0.001);
        Assert.assertEquals(3.333, LuxoftUtils.wordAverageLength("how it works"), 0.001);
        Assert.assertEquals(0, LuxoftUtils.wordAverageLength(""), 0.001);
        Assert.assertEquals(0, LuxoftUtils.wordAverageLength(" "), 0.001);
    }


    @Test
    public void testReverseWords() {
        Assert.assertEquals(" my name is java", LuxoftUtils.reverseWords(" ym eman si avaj"));
        Assert.assertEquals("i can't believe this works!", LuxoftUtils.reverseWords("i t'nac eveileb siht !skrow"));
        Assert.assertEquals("something   like  that ", LuxoftUtils.reverseWords("gnihtemos   ekil  taht "));
        Assert.assertEquals("one  ", LuxoftUtils.reverseWords("eno  "));
        Assert.assertEquals("two", LuxoftUtils.reverseWords("owt"));
        Assert.assertEquals(" ", LuxoftUtils.reverseWords(" "));
    }

    @Test
    public void testGetCharEntries() {
        char[] expectedArray = {'Z', 'a', 'b', 'c'};
        Assert.assertArrayEquals(expectedArray, LuxoftUtils.getCharEntries("aaabcbZZZ   "));
        Assert.assertArrayEquals(new char[]{}, LuxoftUtils.getCharEntries(" "));
        Assert.assertArrayEquals(new char[]{'s', 'A', 'B', 'C', 'S', 'a', 'b', 'c'},
                LuxoftUtils.getCharEntries("aAbBcCssS"));
    }


    @Test
    public void testCalculateOverallArea() {
        List<Figure> figures = new ArrayList<>(3);
        figures.add(new Circle(3.0));       //area is ~28.26
        figures.add(new Square(2.0));       //area is 4.0
        figures.add(new Hexagon(5.0));      //area is ~64,95191
        Assert.assertEquals(97.226239, LuxoftUtils.calculateOverallArea(figures), 0.000001);
    }
}
