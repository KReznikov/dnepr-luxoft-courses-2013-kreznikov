package com.luxoft.dnepr.courses.regular.unit4;


import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class EqualSetTest {

    @Test
    public void baseTest() {
        EqualSet<String> testSet = new EqualSet<>();
        Assert.assertTrue(testSet.add("a"));
        Assert.assertTrue(testSet.add("b"));
        Assert.assertTrue(testSet.add("c"));
        Assert.assertTrue(!testSet.add("a"));
        Assert.assertEquals(3, testSet.size());

        String temp = "for each result: ";

        for (String entry : testSet) {
            temp = temp + entry;
        }

        Assert.assertEquals("for each result: abc", temp);
    }


    @Test
    public void baseNullTest() {
        EqualSet<String> testSet = new EqualSet<>();
        testSet.add(null);
        testSet.add(null);
        testSet.add(null);
        testSet.add(null);
        Assert.assertEquals(1, testSet.size());
    }


    @Test
    public void addAllTest() {
        EqualSet<String> testSet1 = new EqualSet<>();
        testSet1.add("a");
        testSet1.add("b");
        testSet1.add("c");

        EqualSet<String> testSet2 = new EqualSet<>();
        testSet2.add("e");
        testSet2.add("a");
        testSet2.add("b");

        Assert.assertTrue(testSet1.addAll(testSet2));
        Assert.assertTrue(!testSet1.addAll(testSet2));

        Assert.assertEquals(4, testSet1.size());
    }

    @Test
    public void constructorWithParametersTest() {
        List<String> list = new ArrayList<>();
        list.add("a");
        list.add("a");
        list.add("b");
        list.add("c");
        list.add("b");
        list.add("b");
        list.add("c");

        EqualSet<String> testSet = new EqualSet<>(list);
        Assert.assertEquals(3, testSet.size());
    }


    @Test
    public void removeAndClearTest() {
        EqualSet<String> testSet = new EqualSet<>();
        testSet.add("b");
        testSet.add("c");

        Assert.assertTrue(testSet.remove("c"));
        Assert.assertTrue(!testSet.remove("z"));
        Assert.assertEquals(1, testSet.size());

        testSet.clear();
        Assert.assertEquals(0, testSet.size());
        Assert.assertTrue(testSet.isEmpty());
    }

    @Test
    public void toArrayTest() {
        EqualSet<String> testSet = new EqualSet<>();
        testSet.add("a");
        testSet.add("b");
        testSet.add("c");
        Assert.assertArrayEquals(new String[] {"a","b","c"}, testSet.toArray());
    }

    @Test
    public void removeAllTest() {
        EqualSet<String> testSet = new EqualSet<>();
        testSet.add("a");
        testSet.add("b");
        testSet.add("c");

        EqualSet<String> set = new EqualSet<>();
        set.add("d");
        set.add("b");
        set.add("c");
        testSet.removeAll(set);
        Assert.assertEquals(1, testSet.size());
    }

}
