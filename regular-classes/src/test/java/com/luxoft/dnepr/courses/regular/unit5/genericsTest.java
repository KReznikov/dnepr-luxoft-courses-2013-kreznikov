package com.luxoft.dnepr.courses.regular.unit5;

import com.luxoft.dnepr.courses.regular.unit5.dao.EmployeeDaoImpl;
import com.luxoft.dnepr.courses.regular.unit5.dao.IDao;
import com.luxoft.dnepr.courses.regular.unit5.dao.RedisDaoImpl;
import com.luxoft.dnepr.courses.regular.unit5.exceptions.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exceptions.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Employee;
import com.luxoft.dnepr.courses.regular.unit5.model.Redis;
import org.junit.Assert;
import org.junit.Test;


public class genericsTest {

    @Test
    public void baseTest() {
        IDao<Employee> employeeDao = new EmployeeDaoImpl();
        Employee man = new Employee();
        man.setId(12L);
        employeeDao.save(man);

        Assert.assertEquals(man, employeeDao.get(12L));

        IDao<Redis> redisDao = new RedisDaoImpl();
        Redis redis = new Redis();
        redisDao.save(redis);
        Assert.assertEquals((long)redis.getId(),1L);
        redisDao.save(new Redis());

        Assert.assertTrue(redisDao.delete(1));
        Assert.assertTrue(redisDao.delete(2));
        Assert.assertFalse(redisDao.delete(3));


        Employee anotherMan = new Employee(12L,5200);
        try {
            employeeDao.save(anotherMan);
        } catch (UserAlreadyExist e) {
        }

        Employee invalidId = new Employee();
        invalidId.setId(null);
        try {
            employeeDao.update(invalidId);
        } catch (UserNotFound e) {
        }

        Assert.assertEquals(null, employeeDao.get(353L));
    }


    @Test
    public void additionalTest() {

        IDao<Employee> employeeDao = new EmployeeDaoImpl();
        employeeDao.save(new Employee(1L, 12000));

        IDao<Redis> redisDao = new RedisDaoImpl();
        redisDao.save(new Redis(8L, 20));

         Assert.assertFalse(redisDao.delete(1L));
    }

}


