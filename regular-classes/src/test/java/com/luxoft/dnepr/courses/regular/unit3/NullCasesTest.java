package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.FieldFormatException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.NoUserFoundException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.TransactionException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletDoesNotExistException;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


public class NullCasesTest {
    @Test
    public void allNullCasesTest() throws FieldFormatException, NoUserFoundException, TransactionException,
            WalletDoesNotExistException {
        try {
            Bank bankNull = new Bank(null);
        } catch (FieldFormatException e) {
            Assert.assertEquals(e.getMessage(), "Enter valid version number! You trying assign to this field null!");
        }
        Bank bank = new Bank(System.getProperty("java.version"));
        Map<Long, UserInterface> users = new HashMap<>();

        Wallet harrysWallet = new Wallet(1286544L, new BigDecimal("9"), new BigDecimal("10.99"), WalletStatus.ACTIVE);
        User harry = new User(3606464920L, "Harry", harrysWallet);
        users.put(harry.getId(), harry);

        try {
            User john = new User(3376464920L, null, null);
        } catch (FieldFormatException e) {
            Assert.assertEquals(e.getMessage(), "Enter valid user name! You trying assign to this field null!");
        }

        User john = new User(3376464920L, "John", null);
        users.put(john.getId(), john);

        bank.setUsers(users);
        try {
            john.setName(null);
        } catch (FieldFormatException e) {
            Assert.assertEquals(e.getMessage(), "Enter valid user name! You trying assign to this field null!");
        }

        try {
            bank.makeMoneyTransaction(john.getId(), harry.getId(), new BigDecimal("1.00"));
        } catch (WalletDoesNotExistException e) {
            Assert.assertEquals(e.getMessage(), "User John doesn't have any wallet");
        }

        Wallet johnsWallet = new Wallet(1442L, new BigDecimal("0"), new BigDecimal("10.99"), WalletStatus.ACTIVE);
        john.setWallet(johnsWallet);

        try {
            bank.makeMoneyTransaction(john.getId(), harry.getId(), null);
        } catch (FieldFormatException e) {
            Assert.assertEquals(e.getMessage(), "Enter valid amount to transfer! You trying assign to this field null!");
        }
    }
}
