package com.luxoft.dnepr.courses.unit4;

import com.luxoft.dnepr.courses.unit4.model.*;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;


public class LuxoftWorkerStorageTest {
    @Test
    public void testSerialize() throws Exception {
        List<ITWorker> workers = new ArrayList<>();
        workers.add (new Admin("Nikolay","Baskov","2-34-235","Home","m", new String[] {"smoking","drinking"},true));
        workers.add (new Developer ("Filip","Kirkorov","4-55-145","Spoon","m", new String[] {"singing"},38));
        workers.add (new QA ("Alla","Pugacheva","2-12-129","goodTV","f", new String[] {"eating","cooking"},"married"));
        workers.add (new PM ("Sergey","Sosedov","3-69-699","BlueStreet","f", new String[] {"driving"},'A'));
        workers.add (new BA ("Mike","Poplavskiy","7-12-529","goodTV","f", new String[] {"dancing","eating"},105.8));
        LuxoftWorkerStorage.serialize(workers, "D:/serialized.csv");

        List<ITWorker> desWorkers = LuxoftWorkerStorage.deserialize("D:/serialized.csv");
        Assert.assertEquals(workers, desWorkers);

        LuxoftWorkerStorage.serializeToXML(workers, "D:\\file.xml");
        List<ITWorker> desWorkersFromXML = LuxoftWorkerStorage.deserializeFromXML("D:\\file.xml");
        Assert.assertEquals(workers, desWorkersFromXML);

        //Pattern p = Pattern.compile("^.+,firstName="+name+",.+$");
    }
}
