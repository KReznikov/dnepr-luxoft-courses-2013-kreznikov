package com.luxoft.dnepr.courses.regular.unit7;

import java.io.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;


public class Indexer implements Runnable {

    BlockingQueue<File> fileQueue;
    WordStorage wordStorage;
    CountDownLatch latch;

    public Indexer(BlockingQueue<File> fileQueue, WordStorage wordStorage, CountDownLatch latch) {
        this.fileQueue = fileQueue;
        this.wordStorage = wordStorage;
        this.latch = latch;
    }

    public void run() {
        boolean done = false;
        try {
            while (!done) {
                File file = fileQueue.take();
                if (file == FileCrawler.DUMMY) {
                    fileQueue.put(FileCrawler.DUMMY);
                    done = true;
                } else {
                    readFromTxt(file);
                }
            }
        } catch (InterruptedException | IOException e) {
            System.out.println(e);
        }
        latch.countDown();
    }


    private void readFromTxt(File file) throws IOException {
        InputStream is = new FileInputStream(file);
        String input = convertToString(is);
        parse(input);
    }

    private void parse(String input) {
        String[] world = input.split("(?U)[\\W_]+");
        for (String s : world) {
            wordStorage.save(s);
        }
    }

    private static String convertToString(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader in = new BufferedReader(new InputStreamReader(is,"UTF-8"));
        String line;
        while ((line = in.readLine()) != null) {
            sb.append(line).append("$");
        }
        is.close();
        return sb.toString();
    }

}
