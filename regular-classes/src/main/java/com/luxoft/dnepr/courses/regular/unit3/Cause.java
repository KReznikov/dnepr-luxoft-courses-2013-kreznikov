package com.luxoft.dnepr.courses.regular.unit3;

public enum Cause {

    NULL(" You trying assign to this field null!"),
    NEGATIVE(" This field can't be negative");

    private final String text;

    Cause(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

}
