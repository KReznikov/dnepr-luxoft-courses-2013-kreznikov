package com.luxoft.dnepr.courses.regular.unit2;

import java.util.Date;

public class Book extends AbstractProduct {

    public Date publicationDate;

    public Book(String code, String name, double price, Date publicationDate) {
        super(code, name, price);
        this.publicationDate = publicationDate;
    }

    public Book() {
    }

    @Override
    public Book clone() throws CloneNotSupportedException {
        Book clone = (Book) super.clone();
        if (publicationDate != null) {
            clone.publicationDate = (Date) publicationDate.clone();
        }
        return clone;
    }


    @Override
    public boolean equals(Object obj) {
        if (!super.equals(obj) || obj.getClass() != this.getClass()) {
            return false;
        }
        Book book = (Book) obj;
        return !(publicationDate != null ? !publicationDate.equals(book.getPublicationDate()) : book.publicationDate != null);
    }

    @Override
    public int hashCode() {
        int hash = super.hashCode();
        hash = 31 * hash + (null == publicationDate ? 0 : publicationDate.hashCode());
        return hash;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }


}
