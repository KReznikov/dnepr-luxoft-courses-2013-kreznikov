package com.luxoft.dnepr.courses.regular.unit14.view.commands;


import com.luxoft.dnepr.courses.regular.unit14.Vocabulary;

public class InputErrorAction extends CommandAction {

    @Override
    public boolean doAction(Vocabulary vocabulary) {
        printInputError ();
        System.out.print(vocabulary.getRandomWord(false) + " ");
        return true;
    }

    private void printInputError () {
        System.out.println("\nERROR: INVALID COMMAND\n" +
                "-------------------------------------------------\n" +
                "To read list of supported commands enter \"HELP\".\n" +
                "-------------------------------------------------\n");
    }
}
