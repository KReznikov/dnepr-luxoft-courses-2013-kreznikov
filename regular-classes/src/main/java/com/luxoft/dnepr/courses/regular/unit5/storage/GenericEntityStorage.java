package com.luxoft.dnepr.courses.regular.unit5.storage;

import com.luxoft.dnepr.courses.regular.unit5.model.Entity;

import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

public class GenericEntityStorage <T extends Entity> {
    private SortedMap<Long, T> entities = new TreeMap<>();

    public Map<Long, T> getEntities() {
        return entities;
    }

    public Long lastKey() {
        return entities.isEmpty() ? 0L : entities.lastKey();
    }
}
