package com.luxoft.dnepr.courses.unit4.model;


public class Developer extends ITWorker {
    public int age;

    public Developer(String firstName, String lastName, String phone, String company, String gender,
                 String[] skills, int age) {
        super(firstName, lastName, phone, company, gender, skills);
        this.age = age;
    }

    public Developer(){}

    @Override
    public String getUniqueField() {
        return Integer.toString(age);
    }

    public void setUniqueField(String age) {
        this.age = Integer.valueOf(age);
    }

}
