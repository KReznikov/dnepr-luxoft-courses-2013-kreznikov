package com.luxoft.dnepr.courses.unit2.model;

public class Square extends Figure {

    public double side;

    public Square(double side) {
        this.side = side;
    }

    @Override
    public double calculateArea() {
        return side*side;
    }

}
