package com.luxoft.dnepr.courses.unit4.model;


import java.util.Arrays;

public abstract class ITWorker {
    // enum Gender {m, f}

    public String firstName;
    public String lastName;
    public String phone;
    public String company;
    public String gender;
    public String[] skills;

    ITWorker(String firstName, String lastName, String phone, String company, String gender, String[] skills) {  //с двумя разрешенными значениями ('m' или 'f')
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
        this.company = company;
        this.gender = gender;
        this.skills = skills;
    }

    ITWorker() {}

    public String getClassName() {
        return this.getClass().getName();
    }

    public String getName() {
        return this.firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public String getPhone() {
        return this.phone;
    }

    public String getCompany() {
        return this.company;
    }

    public String getGender() {
        return this.gender;
    }

    public String[] getSkills() {
        return this.skills;
    }

    public abstract String getUniqueField();


    public void setName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setSkills(String[] skills) {
        this.skills = skills;
    }

    public abstract void setUniqueField(String uniqueField);

    @Override
    public boolean equals(Object obj) {

        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        ITWorker person = (ITWorker) obj;

        return this.firstName.equals(person.getName())
                && this.lastName.equals(person.getLastName())
                && this.phone.equals(person.getPhone())
                && this.company.equals(person.getCompany())
                && this.gender.equals(person.getGender())
                && Arrays.deepEquals(this.skills, person.getSkills())
                && this.getUniqueField().equals(person.getUniqueField());
    }

}