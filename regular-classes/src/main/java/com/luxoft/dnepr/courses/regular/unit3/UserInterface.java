package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.FieldFormatException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletDoesNotExistException;

interface UserInterface {

    Long getId();
    void setId(Long id) throws FieldFormatException;
    String getName();
    void setName(String name) throws FieldFormatException;
    WalletInterface getWallet() throws WalletDoesNotExistException;
    void setWallet(WalletInterface wallet);

}
