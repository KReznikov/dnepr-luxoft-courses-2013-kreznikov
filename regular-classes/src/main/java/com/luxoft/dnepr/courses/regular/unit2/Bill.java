package com.luxoft.dnepr.courses.regular.unit2;

import java.util.*;

/**
 * Represents a bill.
 * Combines equal {@link Product}s and provides information about total price.
 */
public class Bill {

    List<CompositeProduct> bill = new ArrayList<>();

    /**
     * Appends new instance of product into the bill.
     * Groups all equal products using {@link CompositeProduct}
     *
     * @param product new product
     */
    public void append(Product product) {
        for (CompositeProduct entry : bill) {
            if (product.equals(entry.getFirst())) {
                entry.add(product);
                return;
            }
        }
        createNewGroupAndAddProduct(product);
    }


    private void createNewGroupAndAddProduct(Product product) {
        CompositeProduct group = new CompositeProduct();
        group.add(product);
        bill.add(group);
    }

    /**
     * Calculates total cost of all the products in the bill including discounts.
     *
     * @return
     */
    public double summarize() {
        double sum = 0;
        for (Product product : bill) {
            sum = sum + product.getPrice();
        }
        return sum;
    }

    /**
     * Returns ordered list of products, all equal products are represented by single element in this list.
     * Elements should be sorted by their price in descending order.
     * See {@link CompositeProduct}
     *
     * @return
     */
    public List<Product> getProducts() {
        List<Product> sortedList = new ArrayList<>();
        sortedList.addAll(bill);
        Collections.sort(sortedList, new PriceComparator());

        return sortedList;
    }


    static class PriceComparator implements Comparator<Product> {
        @Override
        public int compare(Product p1, Product p2) {
            if (p1.getPrice() < p2.getPrice()) {
                return 1;
            } else if (p1.getPrice() > p2.getPrice()) {
                return -1;
            } else {
                return 0;
            }
        }
    }


    @Override
    public String toString() {
        List<String> productInfos = new ArrayList<>();
        for (Product product : getProducts()) {
            productInfos.add(product.toString());
        }
        return productInfos.toString() + "\nTotal cost: " + summarize();
    }

}
