package com.luxoft.dnepr.courses.regular.unit6.familytree.io;

import com.luxoft.dnepr.courses.regular.unit6.familytree.FamilyTree;

import java.io.*;


public class IOUtils {


    private IOUtils() {
    }

    public static void save(String filename, FamilyTree familyTree) throws IOException {
        OutputStream os = new FileOutputStream(filename);
        save(os, familyTree);
    }

    public static void save(OutputStream os, FamilyTree familyTree) throws IOException {
        try(ObjectOutputStream oos = new ObjectOutputStream(os))  {
            oos.writeObject(familyTree);
            oos.flush();
        }
    }


    public static FamilyTree load(String filename) throws IOException {
        InputStream is = new FileInputStream(filename);
        return load(is);
    }

    public static FamilyTree load(InputStream is) throws IOException {
        ObjectInputStream ois = new ObjectInputStream(is);
        FamilyTree familyTree;
        try {
            familyTree = (FamilyTree) ois.readObject();
        } catch (Exception e) {
            throw new IOException(e);
        }
        return familyTree;
    }

}
