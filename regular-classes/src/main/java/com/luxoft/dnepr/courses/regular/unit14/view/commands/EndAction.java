package com.luxoft.dnepr.courses.regular.unit14.view.commands;


import com.luxoft.dnepr.courses.regular.unit14.Analyzer;
import com.luxoft.dnepr.courses.regular.unit14.Vocabulary;

import java.util.Set;

public class EndAction extends CommandAction {
    @Override
    public boolean doAction(Vocabulary vocabulary) {
        printTestResult(vocabulary.getSize());
        return false;
    }

    private void printTestResult(int total) {
        System.out.println("\nLINGUISTIC ANALYZE RESULTS:\n" +
                "-------------------------------------------------");
        if (Analyzer.getNumberOfAnswers() > 10) {
            System.out.println("Your estimated vocabulary is " + Analyzer.getEstimate(total) + " words");
            printDetailedStatistic();
        } else {
            System.out.print("Not enough answers, to calculate you result..\n" +
                    "You must answer minimum 10 times (50 recommended)");
        }
    }


    private void printDetailedStatistic() {
        System.out.print("\nList of unknown words: \n" +
                "-------------------------------------------------");
        printAndFormatByWidth(Analyzer.getUnknownWords());
        System.out.print("\n\nList of known words: \n" +
                "-------------------------------------------------");
        printAndFormatByWidth(Analyzer.getKnownWords());
    }

    private void printAndFormatByWidth(Set<String> words) {
        int counter = 0;
        for (String word : words) {
            if (counter % 6 == 0) {
                System.out.println();
            }
            System.out.print(word + " ");
            counter++;
        }

    }

}
