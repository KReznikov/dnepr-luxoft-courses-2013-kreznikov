package com.luxoft.dnepr.courses.unit2;

import com.luxoft.dnepr.courses.unit2.model.Figure;

import java.util.*;

public final class LuxoftUtils {

    /**
     * Function, which sorts array by "merge sort" method.
     *
     * @param array input array of strings
     * @param asc   order of elements in output array. If this value "true" then array will sort in order from lower
     *              to higher
     * @return sorted array
     */

    public static String[] sortArray(String[] array, boolean asc) {
        if (array.length <= 1) return array;

        int half = array.length / 2;
        String[] leftPart = Arrays.copyOf(array, half);
        String[] rightPart = Arrays.copyOfRange(array, half, array.length);

        return mergeArray(sortArray(leftPart, asc), sortArray(rightPart, asc), asc);
    }

    private static String[] mergeArray(String[] leftPt, String[] rightPt, boolean asc) {
        int il = 0;
        int ir = 0;
        int endArrSize = leftPt.length + rightPt.length;
        String[] mergedArray = new String[endArrSize];

        for (int i = 0; i < endArrSize; i++) {
            if (takeFromLeftPt(leftPt, rightPt, il, ir, asc)) {
                mergedArray[i] = leftPt[il++];
            } else {
                mergedArray[i] = rightPt[ir++];
            }
        }
        return mergedArray;
    }

    private static boolean takeFromLeftPt(String[] leftPt, String[] rightPt, int il, int ir, boolean asc) {
        int lenR = rightPt.length;
        int lenL = leftPt.length;

        if (ir < lenR && il < lenL) {
            return (leftPt[il].compareTo(rightPt[ir]) < 0) == asc;
        } else {
            return (il < lenL);
        }
    }


    /**
     * Function, which returns average length of words in string.
     *
     * @param str input string
     * @return number of double type
     */

    public static double wordAverageLength(String str) {
        int totalSpaces = 0;

        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == ' ') {
                totalSpaces++;
            }
        }

        int totalWords = totalSpaces + 1;
        int totalLetters = str.length() - totalSpaces;

        return (double) totalLetters / totalWords;
    }


    /**
     * Function, which reverse every word int the string.
     *
     * @param str input string
     * @return String that contains reversed words
     */

    public static String reverseWords(String str) {
        StringBuilder reversedWords = new StringBuilder();
        StringBuilder temp = new StringBuilder();
        int indexOfNextSpace = 0;

        while (str.contains(" ")) {
            indexOfNextSpace = str.indexOf(" ", indexOfNextSpace);
            temp.append(str.substring(0, indexOfNextSpace)).reverse();
            reversedWords.append(temp).append(" ");
            str = str.substring(indexOfNextSpace + 1);
            temp.delete(0, temp.length());
            indexOfNextSpace = 0;
        }

        temp.append(str.substring(indexOfNextSpace)).reverse();
        reversedWords.append(temp);

        return reversedWords.toString();
    }


    /**
     * Function, which returns array of chars sorted by entry to the input string.
     *
     * @param str input string
     * @return array of chars
     */


    public static char[] getCharEntries(String str) {
        SortedMap<Character, Integer> charEntriesMap = new TreeMap<>();

        for (int i = 0; i < str.length(); i++) {
            char currentLetter = str.charAt(i);
            if (currentLetter != ' ') {
                addCharToMap(charEntriesMap, currentLetter);
            }
        }

        SortedSet<Map.Entry<Character, Integer>> sortedSet = new TreeSet<>(new ValueComparator());
        sortedSet.addAll(charEntriesMap.entrySet());

        return convertToArray(sortedSet);
    }

    private static char[] convertToArray(SortedSet<Map.Entry<Character, Integer>> sortedSet) {
        char[] output = new char[sortedSet.size()];
        int i = 0;
        for (Map.Entry<Character, Integer> entry : sortedSet) {
            output[i] = entry.getKey();
            i++;
        }
        return output;
    }

    private static void addCharToMap(SortedMap<Character, Integer> charEntriesMap, char currentLetter) {
        if (charEntriesMap.get(currentLetter) == null) {
            charEntriesMap.put(currentLetter, 1);
        } else {
            charEntriesMap.put(currentLetter, charEntriesMap.get(currentLetter) + 1);
        }
    }


    static class ValueComparator implements Comparator<Map.Entry<Character, Integer>> {
        @Override
        public int compare(Map.Entry<Character, Integer> e1, Map.Entry<Character, Integer> e2) {
            if (e1.getValue().equals(e2.getValue())) {
                if (e1.getKey() < e2.getKey()) {
                    return -1;
                } else {
                    return 1;
                }
            } else if (e1.getValue() < e2.getValue()) {
                return 1;
            } else {
                return -1;
            }
        }
    }


    /**
     * Function, which calculate area of every figure in array list, then return a sum of them.
     *
     * @param figures input array list of geometric figures
     * @return sum of areas
     */

    public static double calculateOverallArea(List<Figure> figures) {
        double totalArea = 0.0;
        for (Figure f : figures) {
            totalArea = totalArea + f.calculateArea();
        }
        return totalArea;
    }

}


