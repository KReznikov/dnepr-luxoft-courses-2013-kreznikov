package com.luxoft.dnepr.courses.regular.unit3;


import com.luxoft.dnepr.courses.regular.unit3.exceptions.FieldFormatException;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public final class Utils {

    private static DecimalFormat FORMAT;

    private static void formatConstructor() {
        FORMAT = new DecimalFormat("0.00");
        DecimalFormatSymbols custom = new DecimalFormatSymbols();
        custom.setDecimalSeparator('.');
        FORMAT.setDecimalFormatSymbols(custom);
    }


    public static String format(BigDecimal number) {
        if (FORMAT == null) {
            formatConstructor();
        }
        return FORMAT.format(number);
    }


    public static void isNotNegative(BigDecimal number, String field) throws FieldFormatException {
        if (number.signum() < 0) {
            throw new FieldFormatException("Enter valid " + field + " !", Cause.NEGATIVE);
        }
    }

    public static void checkThatNumberIsNotNull(BigDecimal number, String field) throws FieldFormatException {
        if (number == null) {
            throw new FieldFormatException("Enter valid " + field + "!", Cause.NULL);
        }
    }


}
