package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.FieldFormatException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.NoUserFoundException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.TransactionException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletDoesNotExistException;

import java.math.BigDecimal;
import java.util.Map;

public interface BankInterface {

    Map<Long, UserInterface> getUsers();
    void setUsers(Map<Long, UserInterface> users) throws FieldFormatException;
    void makeMoneyTransaction(Long fromUserId, Long toUserId, BigDecimal amount)
            throws NoUserFoundException, TransactionException, FieldFormatException, WalletDoesNotExistException;
}
