package com.luxoft.dnepr.courses.unit1;

public final class LuxoftUtils {

    /**
     * Function, which returns name of the month.
     *
     * @param monthOrder order number of month
     * @param language   output language
     * @return String that contains name of the month according to language and order number
     */

    public static String getMonthName(int monthOrder, String language) {
        if (language == null) {
            return "Unknown Language";
        } else switch (language) {
            case "ru":
                return getRuMonthName(monthOrder);
            case "en":
                return getEnMonthName(monthOrder);
            default:
                return "Unknown Language";
        }
    }

    private static String getRuMonthName(int month) {
        switch (month) {
            case 1:
                return "Январь";
            case 2:
                return "Февраль";
            case 3:
                return "Март";
            case 4:
                return "Апрель";
            case 5:
                return "Май";
            case 6:
                return "Июнь";
            case 7:
                return "Июль";
            case 8:
                return "Август";
            case 9:
                return "Сентябрь";
            case 10:
                return "Октябрь";
            case 11:
                return "Ноябрь";
            case 12:
                return "Декабрь";
            default:
                return "Неизвестный месяц";
        }
    }

    private static String getEnMonthName(int month) {
        switch (month) {
            case 1:
                return "January";
            case 2:
                return "February";
            case 3:
                return "March";
            case 4:
                return "April";
            case 5:
                return "May";
            case 6:
                return "June";
            case 7:
                return "July";
            case 8:
                return "August";
            case 9:
                return "September";
            case 10:
                return "October";
            case 11:
                return "November";
            case 12:
                return "December";
            default:
                return "Unknown Month";
        }
    }


    /**
     * Function, which converts numbers from binary system to decimal. It based on Horner's scheme.
     * If any illegal argument it returns String "Not binary".
     *
     * @param binaryNumber input binary number
     * @return String that contains decimal number
     */

    public static String binaryToDecimal(String binaryNumber) {
        int decimalNumber = 0;
        try {
            if (binaryNumber == null || binaryNumber.isEmpty()) {
                return "Not binary";
            }
            for (int i = 0; i < binaryNumber.length(); i++) {
                int digit = Character.getNumericValue(binaryNumber.charAt(i));
                if (digit == 1 || digit == 0) {
                    decimalNumber = decimalNumber * 2 + digit;
                } else return "Not binary";
            }
        } catch (NumberFormatException e) {
            return "Not binary";
        }
        return String.valueOf(decimalNumber);
    }


    /**
     * Function, which converts numbers from decimal system to binary.
     * If any illegal argument it returns String "Not decimal".
     *
     * @param decimalNumber input decimal number
     * @return String that contains binary number
     */

    public static String decimalToBinary(String decimalNumber) {
        StringBuilder binaryNumber = new StringBuilder("");
        try {
            if (decimalNumber == null || decimalNumber.isEmpty() || decimalNumber.contains("-")) {
                throw new NumberFormatException();
            }
            int temp = Integer.valueOf(decimalNumber);
            while (temp / 2 >= 1) {
                binaryNumber.append(temp % 2);
                temp = temp / 2;
            }
            binaryNumber.append(temp);
        } catch (NumberFormatException e) {
            return "Not decimal";
        }
        return binaryNumber.reverse().toString();
    }


    /**
     * Function, which sorts array by "bubble sort" method.
     *
     * @param array array to sort
     * @param asc   order of elements in output array. If this value "true" then array will sort in order from lower
     *              to higher
     * @return sorted array
     */

    public static int[] sortArray(int[] array, boolean asc) {
        int[] temp = array.clone();
        for (int i = 0; i < temp.length - 1; i++) {
            for (int j = i + 1; j < temp.length; j++) {
                if (compareElements(temp[i], temp[j], asc)) {
                    swap(temp, i, j);
                }
            }
        }
        return temp;
    }

    private static boolean compareElements(int i, int j, boolean asc) {
        return (i > j) == asc;
    }

    private static void swap(int[] arrayToSwap, int i, int j) {
        int temp = arrayToSwap[i];
        arrayToSwap[i] = arrayToSwap[j];
        arrayToSwap[j] = temp;
    }

}
