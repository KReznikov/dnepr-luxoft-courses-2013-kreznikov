package com.luxoft.dnepr.courses.regular.unit6.familytree.impl;

import com.luxoft.dnepr.courses.regular.unit6.familytree.FamilyTree;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Gender;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Person;

import java.io.*;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class FamilyTreeImpl implements FamilyTree {

    private static final long serialVersionUID = 3057396458981676327L;
    private Person root;
    private transient long creationTime;

    private static final String PERSON = "com.luxoft.dnepr.courses.regular.unit6.familytree.Person";
    private static final String GENDER = "com.luxoft.dnepr.courses.regular.unit6.familytree.Gender";

    private FamilyTreeImpl(Person root, long creationTime) {
        this.root = root;
        this.creationTime = creationTime;
    }

    public static FamilyTree create(Person root) {
        return new FamilyTreeImpl(root, System.currentTimeMillis());
    }

    @Override
    public Person getRoot() {
        return root;
    }

    @Override
    public long getCreationTime() {
        return creationTime;
    }


    private void writeObject(ObjectOutputStream output) throws IOException {
        Writer writer = new BufferedWriter(new OutputStreamWriter(output));
        writePerson(writer);
        writer.flush();
    }


    private void readObject(ObjectInputStream input) throws IOException, NoSuchFieldException, IllegalAccessException {
        String inputData = convertToString(input);
        if (!inputData.equals("")) {
            root = createNewPerson(parseToDataMap(inputData));
        }
    }

    private static String convertToString(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader in = new BufferedReader(new InputStreamReader(is));
        String line;
        while ((line = in.readLine()) != null) {
            sb.append(line);
        }
        is.close();
        return sb.substring(9, sb.length() - 2);
    }


    private static Map<String, String> parseToDataMap(String input) {
        StringBuilder sb = new StringBuilder();
        Map<String, String> personData = new HashMap<>();
        parse(input, sb, personData);
        addDataToMap(sb.toString(), personData);
        return personData;
    }


    private static void parse(String input, StringBuilder sb, Map<String, String> personData) {
        int brackets = 0;
        int quote = 0;
        for (int i = 0; i < input.length(); i++) {
            char symbol = input.charAt(i);
            if (isOuterBracket(brackets, symbol)) {
                brackets = checkBrackets(brackets, symbol);
            } else if (isInnerBracket(brackets, symbol)) {
                brackets = checkBrackets(brackets, symbol);
                process(symbol, sb, personData, brackets, quote);
            } else {
                process(symbol, sb, personData, brackets, quote);
                if (symbol == '"') quote++;
            }
        }
    }


    private static void process(char symbol, StringBuilder sb, Map<String, String> treeData, int brackets, int quote) {
        if (isEndOfThisField(symbol, brackets, quote)) {
            addDataToMap(sb.toString(), treeData);
            sb.delete(0, sb.length());
        } else {
            sb.append(symbol);
        }
    }

    private static boolean isEndOfThisField(char symbol, int brackets, int quote) {
        return (',' == symbol && brackets == 0 && quote % 2 == 0);
    }

    private static void addDataToMap(String str, Map<String, String> treeData) {
        int colon = str.indexOf(':');
        String field = str.substring(1, colon - 1);
        String value = str.substring(colon + 1);
        treeData.put(field, value);
    }


    private static Person createNewPerson(Map<String, String> treeData)
            throws NoSuchFieldException, IllegalAccessException {
        Person person = new PersonImpl();
        Class cls = person.getClass();
        for (Map.Entry entry : treeData.entrySet()) {
            Field field = cls.getDeclaredField((String) entry.getKey());
            field.setAccessible(true);
            String fieldContent = (String) entry.getValue();
            if (!isPersonField(field)) {
                fieldContent = fieldContent.substring(1, (fieldContent.length() - 1));
            }
            fieldSetValue(person, field, fieldContent);
        }
        return person;
    }


    private static void fieldSetValue(Person person, Field field, String fieldContent)
            throws NoSuchFieldException, IllegalAccessException {
        String fieldName = field.getType().getName();
        switch (fieldName) {
            case "java.lang.String":
                field.set(person, fieldContent);
                break;
            case GENDER:
                field.set(person, Gender.valueOf(fieldContent));
                break;
            case PERSON:
                field.set(person, createNewPerson(parseToDataMap(fieldContent)));
                break;
            default:
                field.set(person, Integer.valueOf(fieldContent));
        }
    }

    private void writePerson(Writer writer) throws IOException {
        String data;
        try {
            data = convertToJSON(this);
        } catch (Exception e) {
            throw new IOException(e);
        }
        writer.write(data);
    }

    private static String convertToJSON(FamilyTree familyTree) throws IllegalAccessException, NoSuchFieldException {
        StringBuilder dataJSON = new StringBuilder();
        dataJSON.append("{\"root\":{");
        Person rootPerson = familyTree.getRoot();
        if (rootPerson != null) {
            parsePersonClass(rootPerson, dataJSON);
        } else {
            dataJSON.append("}");
        }
        dataJSON.append("}");
        return dataJSON.toString();
    }


    private static void parsePersonClass(Person person, StringBuilder dataJSON) throws IllegalAccessException {
        Class personClass = person.getClass();
        Field[] field = personClass.getDeclaredFields();
        for (int i = 0; i < field.length; i++) {
            field[i].setAccessible(true);
            if (field[i].get(person) != null) {
                appendComa(dataJSON, i);
                appendFieldName(dataJSON, field[i]);
                appendFieldValue(dataJSON, field[i], person);
            }
        }
        dataJSON.append("}");
    }


    private static void appendFieldValue(StringBuilder dataJSON, Field field, Person person)
            throws IllegalAccessException {
        if (isPersonField(field)) {
            dataJSON.append("{");
            parsePersonClass((Person) field.get(person), dataJSON);
        } else {
            dataJSON.append("\"").append(field.get(person).toString()).append("\"");
        }
    }


    private static int checkBrackets(int brackets, int symbol) {
        return (symbol == '{') ? ++brackets : --brackets;
    }

    private static boolean isOuterBracket(int brackets, int symbol) {
        return brackets == 0 && symbol == '{' || brackets == 1 && symbol == '}';
    }

    private static boolean isInnerBracket(int brackets, int symbol) {
        return brackets > 0 && symbol == '{' || brackets > 1 && symbol == '}';
    }

    private static boolean isPersonField(Field field) {
        return field.getName().equals("mother") || field.getName().equals("father");
    }

    private static void appendFieldName(StringBuilder dataJSON, Field field) {
        dataJSON.append("\"").append(field.getName()).append("\"").append(":");
    }

    private static void appendComa(StringBuilder dataJSON, int i) {
        if (i != 0) dataJSON.append(",");
    }


}
