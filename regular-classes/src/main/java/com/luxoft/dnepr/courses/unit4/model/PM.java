package com.luxoft.dnepr.courses.unit4.model;


public class PM extends ITWorker {
    public char rank;

    public PM(String firstName, String lastName, String phone, String company, String gender,
              String[] skills, char rank) {
        super(firstName, lastName, phone, company, gender, skills);
        this.rank = rank;
    }

    public PM(){}

    @Override
    public String getUniqueField() {
        return Character.toString(rank);
    }

    @Override
    public void setUniqueField(String rank) {
        this.rank = rank.charAt(0);
    }
}
