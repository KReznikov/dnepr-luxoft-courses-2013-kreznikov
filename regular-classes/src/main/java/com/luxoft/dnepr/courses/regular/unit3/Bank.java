package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.*;

import java.math.BigDecimal;
import java.util.Map;

public class Bank implements BankInterface {

    private Map<Long, UserInterface> users;

    public Bank(String expectedJavaVersion) throws FieldFormatException {
        String actualJavaVersion = System.getProperty("java.version");

        if (expectedJavaVersion == null) {
            throw new FieldFormatException("Enter valid version number!", Cause.NULL);
        }
        if (!expectedJavaVersion.equals(actualJavaVersion)) {
            throw new IllegalJavaVersionError(actualJavaVersion, expectedJavaVersion, "your current JRE ver. ");
        }
    }

    public void setUsers(Map<Long, UserInterface> users) throws FieldFormatException {
        if (users == null) {
            throw new FieldFormatException("Impossible add users to bank!", Cause.NULL);
        }
        this.users = users;
    }

    public void makeMoneyTransaction(Long fromUserId, Long toUserId, BigDecimal amount)
            throws NoUserFoundException, TransactionException, WalletDoesNotExistException, FieldFormatException {
        checkUserAndWallet(fromUserId);
        checkUserAndWallet(toUserId);
        Utils.checkThatNumberIsNotNull(amount, "amount to transfer");
        Utils.isNotNegative(amount, "amount to transfer");
        validateTransaction(fromUserId, toUserId, amount);
        getUserWallet(fromUserId).withdraw(amount);
        getUserWallet(toUserId).transfer(amount);
    }

    public Map<Long, UserInterface> getUsers() {
        return users;
    }

    private void validateTransaction(Long fromUserId, Long toUserId, BigDecimal amount) throws TransactionException,
            WalletDoesNotExistException {
        try {
            getUserWallet(fromUserId).checkWithdrawal(amount);
            getUserWallet(toUserId).checkTransfer(amount);
        } catch (WalletIsBlockedException e) { // I think it never happens
            throw new TransactionException("Wallet (id " + e.getWalletId() + ") is blocked");
        } catch (InsufficientWalletAmountException e) {
            throw new TransactionException("User '" + getName(fromUserId) + "' has insufficient funds ("
                    + Utils.format(e.getAmountInWallet()) + " < " + Utils.format(e.getAmountToWithdraw()) + ")");
        } catch (LimitExceededException e) {
            throw new TransactionException("User '" + getName(toUserId) + "' wallet limit exceeded ("
                    + Utils.format(e.getAmountInWallet()) + " + " + Utils.format(e.getAmountToTransfer()) + " > "
                    + Utils.format(getUserWallet(toUserId).getMaxAmount()) + ")");
        }
    }

    private String getName(Long userId) {
        return users.get(userId).getName();
    }

    private void checkUserAndWallet(Long userId) throws NoUserFoundException, WalletDoesNotExistException,
            TransactionException {
        if (!users.containsKey(userId)) {
            throw new NoUserFoundException(userId, "User with id " + userId + " not found");
        }
        if (getUserWallet(userId).getStatus() == WalletStatus.BLOCKED) {
            throw new TransactionException("User '" + getName(userId) + "' wallet is blocked");
        }
    }

    private WalletInterface getUserWallet(Long userId) throws WalletDoesNotExistException {
        return users.get(userId).getWallet();
    }


}
