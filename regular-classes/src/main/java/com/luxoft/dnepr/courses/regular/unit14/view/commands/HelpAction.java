package com.luxoft.dnepr.courses.regular.unit14.view.commands;
import com.luxoft.dnepr.courses.regular.unit14.Vocabulary;

public class HelpAction extends CommandAction {
    @Override
    public boolean doAction(Vocabulary vocabulary) {
        System.out.println("\nUSAGE: \n" +
                "----------------------------------\n" +
                "HELP      -prints this message\n" +
                "Y(YES)    -yes, I know this word\n" +
                "N(NO)     -I don't know this word\n" +
                "END       -print results and exit\n" +
                "----------------------------------\n");
        System.out.print(vocabulary.getRandomWord(false) + " ");
        return true;
    }
}
