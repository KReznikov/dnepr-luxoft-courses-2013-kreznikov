package com.luxoft.dnepr.courses.unit4.model;


public class QA extends ITWorker {
    public String maritalStatus;

    public QA(String firstName, String lastName, String phone, String company, String gender,
              String[] skills, String maritalStatus) {
        super(firstName, lastName, phone, company, gender, skills);
        this.maritalStatus = maritalStatus;
    }

    public QA(){}

    @Override
    public String getUniqueField() {
        return maritalStatus;
    }

    @Override
    public void setUniqueField(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

}
