package com.luxoft.dnepr.courses.regular.unit6.familytree.impl;

import com.luxoft.dnepr.courses.regular.unit6.familytree.Gender;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Person;

public class PersonImpl implements Person {

    private int age;
    private String name;
    private String ethnicity;
    private Gender gender;
    private Person mother;
    private Person father;

    public PersonImpl(int age, String name, String ethnicity, Gender gender, Person mother, Person father) {
        this.age = age;
        this.name = name;
        this.ethnicity = ethnicity;
        this.gender = gender;
        this.mother = mother;
        this.father = father;
    }

    public PersonImpl() {
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getEthnicity() {
        return ethnicity;
    }

    @Override
    public Person getFather() {
        return father;
    }

    @Override
    public Person getMother() {
        return mother;
    }

    @Override
    public Gender getGender() {
        return gender;
    }

    @Override
    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEthnicity(String ethnicity) {
        this.ethnicity = ethnicity;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public void setMother(Person mother) {
        this.mother = mother;
    }

    public void setFather(Person father) {
        this.father = father;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PersonImpl)) return false;

        PersonImpl person = (PersonImpl) o;

        if (age != person.age) return false;
        if (ethnicity != null ? !ethnicity.equals(person.ethnicity) : person.ethnicity != null) return false;
        if (father != null ? !father.equals(person.father) : person.father != null) return false;
        if (gender != person.gender) return false;
        if (mother != null ? !mother.equals(person.mother) : person.mother != null) return false;
        if (name != null ? !name.equals(person.name) : person.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = age;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (ethnicity != null ? ethnicity.hashCode() : 0);
        result = 31 * result + (gender != null ? gender.hashCode() : 0);
        result = 31 * result + (mother != null ? mother.hashCode() : 0);
        result = 31 * result + (father != null ? father.hashCode() : 0);
        return result;
    }
}
