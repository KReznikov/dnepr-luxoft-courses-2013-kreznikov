package com.luxoft.dnepr.courses.regular.unit14.view;

import com.luxoft.dnepr.courses.regular.unit14.Vocabulary;
import com.luxoft.dnepr.courses.regular.unit14.view.commands.*;

import java.io.IOException;
import java.io.PrintStream;
import java.util.Scanner;

public class UserInterface {

    private Scanner scanner = new Scanner(System.in);
    private PrintStream out = System.out;
    private Vocabulary vocabulary;

    public UserInterface() {
        printAbout();
        try {
            vocabulary = new Vocabulary();
        } catch (IOException | NullPointerException e) {
            printError(e);
        }
    }

    public void start() {
        out.print(vocabulary.getRandomWord(true) + " ");
        while (scanner.hasNextLine()) {
            String command = scanner.nextLine();
            boolean feedback = execute(command);
            if (!feedback) return;
        }
    }

    private boolean execute(String command) {
        command = command.trim().toLowerCase();
        return CommandAction.getAction(command).doAction(vocabulary);
    }

    private void printAbout() {
        out.println("Linguistic Analyzer [ver.1.0.1] (C) Luxoft 2013\n" +
                "-------------------------------------------------\n" +
                "type \"Y\" beside word if you know translation of" +
                "\nit, else type \"N\". Enter \"END\" to finish program\n"
                + "-------------------------------------------------\n");
    }

    private void printError(Exception ex) {
        out.println("\nERROR: VOCABULARY ISN'T CREATED\n" +
                "-------------------------------------------------");
        if (isNpeException(ex)) {
            out.println("There was not any txt file to create vocabulary. ");
        } else {
            out.println("The problem is occurred when reading txt files. ");
        }
        out.println("-------------------------------------------------");
    }

    private boolean isNpeException(Exception e) {
        return e instanceof NullPointerException;
    }

}
