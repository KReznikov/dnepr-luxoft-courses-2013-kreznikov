package com.luxoft.dnepr.courses.regular.unit14.view.commands;


import com.luxoft.dnepr.courses.regular.unit14.Analyzer;
import com.luxoft.dnepr.courses.regular.unit14.Vocabulary;

public class YesAction extends CommandAction {

    @Override
    public boolean doAction(Vocabulary vocabulary) {
        Analyzer.acceptAnswer(vocabulary.getRandomWord(false), true);
        System.out.print(vocabulary.getRandomWord(true) + " ");
        return true;
    }
}
