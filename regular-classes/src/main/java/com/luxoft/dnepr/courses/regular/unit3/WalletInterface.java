package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.FieldFormatException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.InsufficientWalletAmountException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.LimitExceededException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletIsBlockedException;

import java.math.BigDecimal;

interface WalletInterface {

    Long getId();
    void setId(Long id) throws FieldFormatException;
    BigDecimal getAmount();
    void setAmount(BigDecimal amount) throws FieldFormatException;
    WalletStatus getStatus();
    void setStatus(WalletStatus status);
    BigDecimal getMaxAmount();
    void setMaxAmount(BigDecimal maxAmount) throws FieldFormatException;
    void checkWithdrawal(BigDecimal amountToWithdraw) throws WalletIsBlockedException, InsufficientWalletAmountException;
    void withdraw(BigDecimal amountToWithdraw);
    void checkTransfer(BigDecimal amountToTransfer) throws WalletIsBlockedException, LimitExceededException;
    void transfer(BigDecimal amountToTransfer);
}
