package com.luxoft.dnepr.courses.unit4;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.*;

import java.lang.reflect.Field;
import java.util.*;
import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.luxoft.dnepr.courses.unit4.model.*;

public class LuxoftWorkerStorage {

    public static void serialize(List<ITWorker> workers, String absolutePathToFile) {
        File file = new File(absolutePathToFile);
        try {
            String data = convertToCSV(workers);
            PrintWriter out = new PrintWriter(file.getAbsoluteFile());
            out.print(data);
            out.close();
        } catch (IOException | IllegalAccessException e) {
            System.out.println(e);
        }
    }

    private static String convertToCSV(List<ITWorker> workers) throws IllegalAccessException {
        StringBuilder entriesInCSV = new StringBuilder();
        for (ITWorker worker : workers) {
            Class workerClass = worker.getClass();
            Field[] publicFields = workerClass.getFields();
            entriesInCSV.append(worker.getClassName()).append(",");

            for (Field field : publicFields) {
                entriesInCSV.append(field.getName()).append("=");
                Class fieldType = field.getType();
                if (fieldType.getName().equals("[Ljava.lang.String;")) {
                    entriesInCSV.append(arrayInString((String[]) field.get(worker)));
                } else {
                    entriesInCSV.append(field.get(worker).toString());
                }
                entriesInCSV.append(",");
            }
            entriesInCSV.append("\n");
        }
        return entriesInCSV.toString();
    }


    public static List<ITWorker> deserialize(String absolutePathToFile) {
        List<ITWorker> workersDeserialized = new ArrayList<>();
        File file = new File(absolutePathToFile);

        try (BufferedReader in = new BufferedReader(new FileReader(file.getAbsoluteFile()))) {
            String oneLine;
            while ((oneLine = in.readLine()) != null) {
                workersDeserialized.add(parseLine (oneLine));
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return workersDeserialized;
    }

    private static ITWorker parseLine (String line) {
        int indexOfNextSeparator = line.indexOf(",", 0);
        String className = line.substring(0, indexOfNextSeparator);
        line = line.substring(indexOfNextSeparator + 1);
        return createNewObject(className, extractClassData(line));
    }


    List<ITWorker> getWorkersWithFirstname(String absolutePathToFile, String pattern) {
        List<ITWorker> workersDeserialized = new ArrayList<>();
        File file = new File(absolutePathToFile);

        try (BufferedReader in = new BufferedReader(new FileReader(file.getAbsoluteFile()))) {
            String oneLine;
            while ((oneLine = in.readLine()) != null) {
                if (testForMatch(oneLine, pattern)) {
                    workersDeserialized.add(parseLine (oneLine));
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return workersDeserialized;
    }


    public List<ITWorker> getWorkersWithFirstnameAndCompany(String absolutePathToFile, String firstnamePattern,
                                                            String companyPattern) {
        List<ITWorker> workersDeserialized = new ArrayList<>();
        File file = new File(absolutePathToFile);

        try (BufferedReader in = new BufferedReader(new FileReader(file.getAbsoluteFile()))) {
            String oneLine;
            while ((oneLine = in.readLine()) != null) {
                if (testForMatch(oneLine, firstnamePattern)&&testForMatch(oneLine, companyPattern)) {
                    workersDeserialized.add(parseLine (oneLine));
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return workersDeserialized;
    }


    public List<ITWorker> getWorkersWithFirstnameOrCompany(String absolutePathToFile, String firstnamePattern,
                                                           String companyPattern) {
        List<ITWorker> workersDeserialized = new ArrayList<>();
        File file = new File(absolutePathToFile);

        try (BufferedReader in = new BufferedReader(new FileReader(file.getAbsoluteFile()))) {
            String oneLine;
            while ((oneLine = in.readLine()) != null) {
                if (testForMatch(oneLine, firstnamePattern)||testForMatch(oneLine, companyPattern)) {
                    workersDeserialized.add(parseLine (oneLine));
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return workersDeserialized;
    }


    public static boolean testForMatch(String string, String pattern) {
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(string);
        return m.matches();
    }

    private static Map<String, String> extractClassData(String string) {
        Map<String, String> classData = new HashMap<>();
        int indexOfNextSeparator = 0;
        while (string.contains(",")) {
            indexOfNextSeparator = string.indexOf(",", indexOfNextSeparator);
            String key = string.substring(0, string.indexOf("="));
            String value = string.substring(string.indexOf("=") + 1, indexOfNextSeparator);
            classData.put(key, value);
            string = string.substring(indexOfNextSeparator + 1);
            indexOfNextSeparator = 0;
        }
        return classData;
    }

    private static String arrayInString(String[] array) {
        StringBuilder temp = new StringBuilder();
        int lastElement = array.length - 1;
        for (int i = 0; i < lastElement; i++) {
            temp.append(array[i]).append("$");
        }
        temp.append(array[lastElement]);
        return temp.toString();
    }

    private static String[] stringInArray(String str) {
        List<String> values = new ArrayList<>();
        int indexOfNextSeparator = 0;
        while (str.contains("$")) {
            indexOfNextSeparator = str.indexOf("$", indexOfNextSeparator);
            values.add(str.substring(0, indexOfNextSeparator));
            str = str.substring(indexOfNextSeparator + 1);
            indexOfNextSeparator = 0;
        }
        values.add(str.substring(indexOfNextSeparator));

        return values.toArray(new String[values.size()]);
    }

    /**
     * Function, which serialize data to XML.
     *
     * @param absolutePathToFile path where method will create XML file
     * @param workers            list of objects to serialize
     */

    public static void serializeToXML(List<ITWorker> workers, String absolutePathToFile) {

        try {

            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            Document doc = docBuilder.newDocument();

            Element rootElement = doc.createElement("ITWorkerXML");
            doc.appendChild(rootElement);

            for (ITWorker worker : workers) {
                Element className = doc.createElement(worker.getClassName());
                rootElement.appendChild(className);

                Class workerClass = worker.getClass();
                Field[] publicFields = workerClass.getFields();

                for (Field field : publicFields) {
                    Element tagName = doc.createElement(field.getName());
                    Class fieldType = field.getType();
                    if (fieldType.getName().equals("[Ljava.lang.String;")) {
                        tagName.appendChild(doc.createTextNode(arrayInString((String[]) field.get(worker))));
                    } else {
                        tagName.appendChild(doc.createTextNode(field.get(worker).toString()));
                    }
                    className.appendChild(tagName);
                }
            }

            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(absolutePathToFile));

            transformer.transform(source, result);

            System.out.println("File saved!");

        } catch (ParserConfigurationException | TransformerException | IllegalAccessException e) {
            System.out.println(e);
        }
    }

    /**
     * Function, which deserialize data from XML.
     *
     * @param absolutePathToFile path to XML file (include name)
     * @return list with all desearialized objects
     */

    public static List<ITWorker> deserializeFromXML(String absolutePathToFile) {
        List<ITWorker> deserializedFromXML = new ArrayList<>();
        Map<String, String> classData = new HashMap<>();
        try {

            File file = new File(absolutePathToFile);
            DocumentBuilder dBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = dBuilder.parse(file);

            NodeList classesList = doc.getFirstChild().getChildNodes();

            for (int i = 0; i < classesList.getLength(); i++) {
                Node tempNode = classesList.item(i);
                String className = tempNode.getNodeName();

                NodeList fieldsList = tempNode.getChildNodes();
                for (int j = 0; j < fieldsList.getLength(); j++) {
                    Node temp = fieldsList.item(j);
                    classData.put(temp.getNodeName(), temp.getTextContent());
                }

                deserializedFromXML.add(createNewObject(className, classData));
                classData.clear();
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return deserializedFromXML;
    }


    /**
     * Function, which creates object from input data and returns it.
     *
     * @param className full class name
     * @param classData map where key is field of instance
     * @return instance of ITWorker class
     */

    private static ITWorker createNewObject(String className, Map<String, String> classData) {
        ITWorker clsInstance = null;
        try {
            Class<?> cls = Class.forName(className);
            clsInstance = (ITWorker) cls.newInstance();

            for (Map.Entry entry : classData.entrySet()) {
                Field field = cls.getField((String) entry.getKey());
                String fieldContent = (String) entry.getValue();

                fieldSetValue(clsInstance, field, fieldContent);

            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchFieldException e) {
            System.out.println(e);
        }
        return clsInstance;
    }

    private static void fieldSetValue(ITWorker worker, Field field, String fieldContent) {
        Class fieldType = field.getType();
        String fieldName = fieldType.getName();

        try {
            switch (fieldName) {
                case "java.lang.String":
                    field.set(worker, fieldContent);
                    break;
                case "[Ljava.lang.String;":
                    field.set(worker, stringInArray(fieldContent));
                    break;
                case "boolean":
                    field.set(worker, Boolean.valueOf(fieldContent));
                    break;
                case "char":
                    field.set(worker, fieldContent.charAt(0));
                    break;
                case "double":
                    field.set(worker, Double.valueOf(fieldContent));
                    break;
                case "int":
                    field.set(worker, Integer.valueOf(fieldContent));
                    break;
            }
        } catch (IllegalAccessException e) {
            System.out.println(e);
        }
    }


}