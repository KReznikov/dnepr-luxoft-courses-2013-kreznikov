package com.luxoft.dnepr.courses.unit4.model;


public class Admin extends ITWorker {

    public boolean beard;

    public Admin(String firstName, String lastName, String phone, String company, String gender,
                 String[] skills, boolean beard) {
        super(firstName, lastName, phone, company, gender, skills);
        this.beard = beard;
    }

    public Admin(){}

    @Override
    public String getUniqueField() {
        return Boolean.toString(beard);
    }

    @Override
    public void setUniqueField(String beard) {
        this.beard = Boolean.valueOf(beard);
    }

}
