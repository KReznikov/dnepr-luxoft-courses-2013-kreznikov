package com.luxoft.dnepr.courses.regular.unit14;

import java.util.HashSet;
import java.util.Set;

public class Analyzer {

    private static Set<String> knownWords = new HashSet<>();
    private static Set<String> unknownWords = new HashSet<>();

    public static int getEstimate(int total) {
        return total * (knownWords.size() + 1) / (knownWords.size() + unknownWords.size() + 1);
    }

    public static int getNumberOfAnswers() {
        return knownWords.size() + unknownWords.size();
    }

    public static Set<String> getKnownWords() {
        return knownWords;
    }

    public static Set<String> getUnknownWords() {
        return unknownWords;
    }

    public static void acceptAnswer(String word, Boolean answer) {
        if (answer) {
            knownWords.add(word);
        } else {
            unknownWords.add(word);
        }
    }

}
