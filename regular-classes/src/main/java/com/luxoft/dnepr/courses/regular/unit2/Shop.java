package com.luxoft.dnepr.courses.regular.unit2;

import java.util.GregorianCalendar;

/**
 * Main entry point to the program 'Shop'.
 */
public class Shop {
    public static void main(String[] args) throws CloneNotSupportedException {
        ProductFactory productFactory = new ProductFactory();

        Bill bill = new Bill();

        Bread bread = productFactory.createBread("bread1", "White fresh bread", 10, 1.5);
        bill.append(bread);
        Bread anotherBread = (Bread) bread.clone();
        anotherBread.setPrice(15);
        bill.append(anotherBread);

        Beverage cola = productFactory.createBeverage("beverage1", "Coca-cola", 5, true);
        bill.append(cola);
        bill.append(cola.clone());
        bill.append(cola.clone());

        Book javaBook = productFactory.createBook("book1", "Thinking in Java", 200,
                new GregorianCalendar(2006, 0, 1).getTime());
        bill.append(javaBook);

        System.out.println(bill);
    }
}
