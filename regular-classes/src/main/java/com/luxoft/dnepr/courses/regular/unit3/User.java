package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.FieldFormatException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletDoesNotExistException;

public class User implements UserInterface {
    private Long id;
    private String name;
    private WalletInterface wallet;

    public User(Long id, String name, WalletInterface wallet) throws FieldFormatException {
        checkThatIdIsNotNull(id);
        checkThatNameIsNotNull(name);
        this.id = id;
        this.name = name;
        this.wallet = wallet;
    }

    private void checkThatNameIsNotNull(String name) throws FieldFormatException {
        if (name == null) {
            throw new FieldFormatException("Enter valid user name!", Cause.NULL);
        }
    }

    private void checkThatIdIsNotNull(Long id) throws FieldFormatException {
        if (id == null) {
            throw new FieldFormatException("Enter valid user id!", Cause.NULL);
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) throws FieldFormatException {
        checkThatIdIsNotNull(id);
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws FieldFormatException {
        checkThatNameIsNotNull(name);
        this.name = name;
    }

    public WalletInterface getWallet() throws WalletDoesNotExistException {
        if (wallet == null) {
            throw new WalletDoesNotExistException("User " + name + " doesn't have any wallet");
        }
        return wallet;
    }

    public void setWallet(WalletInterface wallet) {
        this.wallet = wallet;
    }
}