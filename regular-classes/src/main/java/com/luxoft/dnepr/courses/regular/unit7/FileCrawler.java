package com.luxoft.dnepr.courses.regular.unit7;

import java.io.*;
import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.regex.Pattern;


/**
 * Crawls files and directories, searches for text files and counts word occurrences.
 */
public class FileCrawler {

    private WordStorage wordStorage = new WordStorage();

    private final List<File> processedFiles = new ArrayList<>();
    private final BlockingQueue<File> fileQueue;
    private String rootFolder;
    private int maxNumberOfThreads;
    public static Pattern filterPattern = Pattern.compile("^.+\\.txt$");
    public static File DUMMY = new File("");

    public FileCrawler(String rootFolder, int maxNumberOfThreads) {
        if (maxNumberOfThreads < 2) {
            this.maxNumberOfThreads = 1;
            fileQueue= new LinkedBlockingQueue<>();
        } else {
            this.maxNumberOfThreads = maxNumberOfThreads;
            fileQueue= new LinkedBlockingQueue<>(maxNumberOfThreads*2);
        }
        this.rootFolder = rootFolder;
    }

    /**
     * Performs crawling using multiple threads.
     * This method should wait until all parallel tasks are finished.
     *
     * @return FileCrawlerResults
     */
    public FileCrawlerResults execute() {
        CountDownLatch latch = new CountDownLatch(maxNumberOfThreads);

        if (maxNumberOfThreads > 1) {
            new Thread(new Producer(fileQueue, rootFolder, processedFiles, latch)).start();
            for (int i = 1; i < maxNumberOfThreads; i++) {
                new Thread(new Indexer(fileQueue, wordStorage, latch)).start();
            }
        } else {
            new Producer(fileQueue, rootFolder, processedFiles, latch).run();
            new Indexer(fileQueue, wordStorage, latch).run();
        }

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return new FileCrawlerResults(processedFiles, wordStorage.getWordStatistics());
    }

    public static void main(String args[]) {
        String rootDir = args.length > 0 ? args[0] : System.getProperty("user.home");
        FileCrawler crawler = new FileCrawler(rootDir, 10);
        crawler.execute();
    }

}
