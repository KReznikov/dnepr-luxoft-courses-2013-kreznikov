package com.luxoft.dnepr.courses.regular.unit5.storage;

import com.luxoft.dnepr.courses.regular.unit5.model.Entity;

import java.util.*;

public class EntityStorage {
    private final static Map<Class, GenericEntityStorage> storage = new HashMap<>();

    private EntityStorage() {
    }

    /**
     * This method returns entity storage map of specific class.
     *
     * @param type this parameter represents class that accesses to the storage
     */

    public static <T extends Entity> Map<Long, Entity> getEntities(Class<T> type) {
        if (!storage.containsKey(type)) {
            storage.put(type, new GenericEntityStorage());
        }
        return storage.get(type).getEntities();
    }

    public static <T extends  Entity> long getMaxId(Class<T> type) {
        return storage.containsKey(type) ? storage.get(type).lastKey() : 0L;
    }
}
