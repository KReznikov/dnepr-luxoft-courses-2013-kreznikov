package com.luxoft.dnepr.courses.regular.unit6.familytree;

public enum Gender {
	
	MALE, FEMALE;
}
