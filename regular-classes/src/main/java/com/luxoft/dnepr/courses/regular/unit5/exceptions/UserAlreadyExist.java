package com.luxoft.dnepr.courses.regular.unit5.exceptions;


public class UserAlreadyExist extends RuntimeException {

    public UserAlreadyExist(String message) {
        super(message);
    }
}
