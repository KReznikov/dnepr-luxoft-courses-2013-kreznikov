package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.*;

import java.math.BigDecimal;

public class Wallet implements WalletInterface {

    Long id;
    BigDecimal amount;
    BigDecimal maxAmount;
    WalletStatus status;

    public Wallet(Long id, BigDecimal amount, BigDecimal maxAmount, WalletStatus walletStatus)
            throws FieldFormatException {
        Utils.checkThatNumberIsNotNull(amount, "amount");
        Utils.checkThatNumberIsNotNull(maxAmount, "wallet limit");
        Utils.isNotNegative(maxAmount, "wallet limit");
        checkThatIdIsNotNull(id);
        this.id = id;
        this.amount = amount;
        this.maxAmount = maxAmount;
        this.status = walletStatus;
    }






    private void checkThatIdIsNotNull(Long id) throws FieldFormatException {
        if (id == null) {
            throw new FieldFormatException("Enter valid wallet id!", Cause.NULL);
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) throws FieldFormatException {
        checkThatIdIsNotNull(id);
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) throws FieldFormatException {
        Utils.checkThatNumberIsNotNull(amount, "amount");
        this.amount = amount;
    }

    public WalletStatus getStatus() {
        return status;
    }

    public void setStatus(WalletStatus status) {
        this.status = status;
    }

    public BigDecimal getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(BigDecimal maxAmount) throws FieldFormatException {
        Utils.checkThatNumberIsNotNull(maxAmount, "wallet limit");
        this.maxAmount = maxAmount;
    }


    private void isBlocked() throws WalletIsBlockedException {
        if (status == WalletStatus.BLOCKED) {
            throw new WalletIsBlockedException(id, "User wallet (id " + id + ") is blocked");
        }
    }

    private boolean isLimitExceeded(BigDecimal amountToTransfer) {
        return (amount.add(amountToTransfer).compareTo(maxAmount) == 1);
    }

    public void checkWithdrawal(BigDecimal amountToWithdraw) throws WalletIsBlockedException, InsufficientWalletAmountException {
        isBlocked();
        if (amount.compareTo(amountToWithdraw) == -1) {
            throw new InsufficientWalletAmountException(id, amountToWithdraw, amount, "Not enough amount to withdraw");
        }
    }

    public void withdraw(BigDecimal amountToWithdraw) {
        amount = amount.subtract(amountToWithdraw);
    }

    public void checkTransfer(BigDecimal amountToTransfer) throws WalletIsBlockedException, LimitExceededException {
        isBlocked();
        if (isLimitExceeded(amountToTransfer)) {
            throw new LimitExceededException(id, amountToTransfer, amount, "Limit of your wallet exceeded");
        }
    }

    public void transfer(BigDecimal amountToTransfer) {
        amount = amount.add(amountToTransfer);
    }
}