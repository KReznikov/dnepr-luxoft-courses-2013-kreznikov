package com.luxoft.dnepr.courses.unit4.model;


public class BA extends ITWorker {
    public double IQ;

    public BA(String firstName, String lastName, String phone, String company, String gender,
                     String[] skills, double IQ) {
        super(firstName, lastName, phone, company, gender, skills);
        this.IQ = IQ;
    }

    public BA(){}

    @Override
    public String getUniqueField() {
        return Double.toString(IQ);
    }

    @Override
    public void setUniqueField(String IQ) {
        this.IQ = Double.valueOf(IQ);
    }

}
