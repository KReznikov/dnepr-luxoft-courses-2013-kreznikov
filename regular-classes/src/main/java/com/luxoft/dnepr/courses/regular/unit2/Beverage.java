package com.luxoft.dnepr.courses.regular.unit2;


public class Beverage extends AbstractProduct {

    private boolean nonAlcoholic;

    public Beverage(String code, String name, double price, boolean nonAlcoholic) {
        super(code, name, price);
        this.nonAlcoholic = nonAlcoholic;
    }

    public Beverage() {
    }

    public boolean isNonAlcoholic() {
        return nonAlcoholic;
    }


    @Override
    public boolean equals(Object obj) {
        if (!super.equals(obj) || obj.getClass() != this.getClass()) {
            return false;
        }
        Beverage beverage = (Beverage) obj;
        return nonAlcoholic == beverage.isNonAlcoholic();
    }

    @Override
    public int hashCode() {
        int hash = super.hashCode();
        hash = 31 * hash + (nonAlcoholic ? 1231 : 1237);
        return hash;
    }

}
