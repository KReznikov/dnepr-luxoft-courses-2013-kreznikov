package com.luxoft.dnepr.courses.regular.unit3.exceptions;


import com.luxoft.dnepr.courses.regular.unit3.Cause;

public class FieldFormatException extends Exception {

    public FieldFormatException(String message, Cause exceptionCause) {
        super(message + exceptionCause.getText());
    }
}
