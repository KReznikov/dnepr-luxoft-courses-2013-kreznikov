package com.luxoft.dnepr.courses.regular.unit7;

import java.io.File;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.regex.Matcher;


public class Producer implements Runnable {

    private BlockingQueue<File> fileQueue;
    private String rootFolder;
    List<File> processedFiles;
    CountDownLatch latch;

    public Producer(BlockingQueue<File> fileQueue, String rootFolder, List<File> processedFiles, CountDownLatch latch) {
        this.fileQueue = fileQueue;
        this.rootFolder = rootFolder;
        this.processedFiles = processedFiles;
        this.latch = latch;
    }

    public void run() {
        searchForTXTFiles(rootFolder);
        try {
            fileQueue.put(FileCrawler.DUMMY); // this file says that this thread finished.
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        latch.countDown();
    }


    private void searchForTXTFiles(String args) {
        File list[] = new File(args).listFiles();
        if (list == null) return;
        for (File file : list) {
            processFile(file);
        }
    }

    private void processFile(File file) {
        if (file.isDirectory()) {
            searchForTXTFiles(file.getPath());
        } else {
            if (isTextFile(file.getName())) {
                processedFiles.add(file);
                try {
                    fileQueue.put(file);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private boolean isTextFile(String string) {
        Matcher m = FileCrawler.filterPattern.matcher(string);
        return m.matches();
    }


}

