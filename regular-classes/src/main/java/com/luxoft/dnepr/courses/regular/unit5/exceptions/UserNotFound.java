package com.luxoft.dnepr.courses.regular.unit5.exceptions;

public class UserNotFound extends RuntimeException {

    public UserNotFound(String message) {
        super(message);
    }
}
