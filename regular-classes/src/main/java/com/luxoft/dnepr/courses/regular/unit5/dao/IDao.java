package com.luxoft.dnepr.courses.regular.unit5.dao;


import com.luxoft.dnepr.courses.regular.unit5.model.Entity;

public interface IDao<E extends Entity> {
    E save (E e);
    E update (E e);
    E get (long id);
    boolean delete (long id);
}
