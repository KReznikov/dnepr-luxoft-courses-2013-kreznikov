package com.luxoft.dnepr.courses.regular.unit5.dao;


import com.luxoft.dnepr.courses.regular.unit5.exceptions.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exceptions.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Entity;
import com.luxoft.dnepr.courses.regular.unit5.storage.*;

public abstract class AbstractDao<E extends Entity> implements IDao<E> {

    private Class<E> type;

    public AbstractDao(Class<E> type) {
        this.type = type;
    }

    private boolean isIdExist(Long id) {
        return EntityStorage.getEntities(type).containsKey(id);
    }

    public E save(E e) {
        Long id = e.getId();
        if (id == null) {
            id = EntityStorage.getMaxId(type) + 1;
            e.setId(id);
        } else if (isIdExist(id)) {
            throw new UserAlreadyExist("this id is already in use");
        }
        EntityStorage.getEntities(type).put(id, e);
        return e;
    }

    public E update(E e) {
        Long id = e.getId();
        if (id == null || !isIdExist(id)) {
            throw new UserNotFound("there is no this id in storage");
        }
        EntityStorage.getEntities(type).put(e.getId(), e);
        return e;
    }

    public E get(long id) {
        return (E) EntityStorage.getEntities(type).get(id);
    }

    public boolean delete(long id) {
        return EntityStorage.getEntities(type).remove(id) != null;
    }

}

