package com.luxoft.dnepr.courses.regular.unit2;

public abstract class AbstractProduct implements Product, Cloneable {
    public String code;
    public String name;
    public double price;

    AbstractProduct(String code, String name, double price) {
        this.code = code;
        this.name = name;
        this.price = price;
    }

    AbstractProduct() {
    }


    @Override
    public AbstractProduct clone() throws CloneNotSupportedException {
        return (AbstractProduct) super.clone();
    }


    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + (null == code ? 0 : code.hashCode());
        hash = 31 * hash + (null == name ? 0 : name.hashCode());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }

        AbstractProduct product = (AbstractProduct) obj;
        if (code != null ? !code.equals(product.code) : product.code != null) return false;
        if (name != null ? !name.equals(product.name) : product.name != null) return false;

        return true;
    }


    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

}
