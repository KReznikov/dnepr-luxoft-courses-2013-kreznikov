package com.luxoft.dnepr.courses.regular.unit14;

import com.luxoft.dnepr.courses.regular.unit14.view.UserInterface;

import java.io.IOException;

public class LinguisticAnalyzer {
    public static void main(String[] args) throws IOException {
        UserInterface ui = new UserInterface();
        ui.start();
    }
}
