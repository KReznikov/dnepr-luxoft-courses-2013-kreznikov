package com.luxoft.dnepr.courses.regular.unit3.exceptions;


public class WalletDoesNotExistException extends Exception {
    public WalletDoesNotExistException(String message) {
        super(message);
    }
}
