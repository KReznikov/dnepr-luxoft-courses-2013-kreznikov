package com.luxoft.dnepr.courses.regular.unit14.view.commands;

import com.luxoft.dnepr.courses.regular.unit14.Vocabulary;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public abstract class CommandAction {

    private static final Map<String, CommandAction> ACTIONS;

    static {
        Map<String, CommandAction> result = new HashMap<>();
        YesAction yesAction = new YesAction();
        NoAction noAction = new NoAction();

        result.put("no", noAction);
        result.put("n", noAction);
        result.put("yes", yesAction);
        result.put("y", yesAction);
        result.put("end", new EndAction());
        result.put("help", new HelpAction());
        result.put("inputError", new InputErrorAction());

        ACTIONS = Collections.unmodifiableMap(result);
    }

    public static CommandAction getAction(String command) {
        return ACTIONS.containsKey(command) ? ACTIONS.get(command) : ACTIONS.get("inputError");
    }

    public abstract boolean doAction(Vocabulary vocabulary);
}
