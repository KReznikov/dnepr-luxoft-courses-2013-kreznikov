package com.luxoft.dnepr.courses.regular.unit14;

import java.io.*;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Vocabulary {

    private String[] vocabulary;
    private String randomWold;
    private static Pattern romanNumerals = Pattern.compile("^M{0,4}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$");

    public Vocabulary() throws IOException {
        vocabulary = createVocabulary();
        randomWold = vocabulary[0];
    }

    public String getRandomWord(boolean generateNew) {
        if (generateNew) {
            int randomIndex = (int) (Math.random() * getSize() - 1);
            randomWold = vocabulary[randomIndex];
        }
        return randomWold;
    }

    public int getSize() {
        return vocabulary.length;
    }

    private String[] createVocabulary() throws IOException {
        InputStream is = getClass().getResourceAsStream("/unit14/sonnets.txt");
        String input = convertToString(is);
        Set<String> uniqueWorlds = new HashSet<>();
        String[] words = input.split("[\\W_\\d]+");
        for (String word : words) {
            addIfAcceptableWord(word, uniqueWorlds);
        }
        return uniqueWorlds.toArray(new String[uniqueWorlds.size()]);
    }

    private void addIfAcceptableWord(String word, Set<String> uniqueWorlds) {
        if (word.length() > 3 & isNotRomanNumeral(word)) {
            uniqueWorlds.add(word.toLowerCase());
        }
    }

    private boolean isNotRomanNumeral(String word) {
        Matcher m = romanNumerals.matcher(word);
        return !m.matches();
    }

    private static String convertToString(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader in = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        String line;
        while ((line = in.readLine()) != null) {
            sb.append(line).append(" ");
        }
        is.close();
        return sb.toString();
    }

}
