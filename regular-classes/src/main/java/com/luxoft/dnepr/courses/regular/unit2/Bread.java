package com.luxoft.dnepr.courses.regular.unit2;

public class Bread extends AbstractProduct {

    private double weight;

    public Bread(String code, String name, double price, double weight) {
        super(code, name, price);
        this.weight = weight;
    }

    public Bread() {
    }

    @Override
    public boolean equals(Object obj) {
        if (!super.equals(obj) || obj.getClass() != this.getClass()) {
            return false;
        }
        Bread bread = (Bread) obj;
        return weight == bread.getWeight();
    }

    @Override
    public int hashCode() {
        int hash = super.hashCode();
        hash = 31 * hash + (int) weight;
        return hash;
    }


    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }


}
