SELECT makers.maker_name, laptop.speed
FROM product
JOIN makers ON product.maker_id = makers.maker_id
JOIN laptop ON laptop.model = product.model
WHERE laptop.hd >= 10.0