SELECT DISTINCT a.model, COALESCE(b.price, c.price, d.price) AS 'price'
FROM ((((product a LEFT JOIN PC b ON a.model = b.model
 ) LEFT JOIN 
 laptop c ON a.model = c.model
 ) LEFT JOIN 
 printer d ON a.model = d.model
 ) LEFT JOIN 
 makers m ON a.maker_id = m.maker_id
 )
WHERE m.maker_name = 'B';