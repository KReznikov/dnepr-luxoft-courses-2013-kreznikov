SELECT type, product.model, speed
FROM laptop
JOIN product ON product.model = laptop.model
WHERE speed < (SELECT MIN(speed) FROM pc)