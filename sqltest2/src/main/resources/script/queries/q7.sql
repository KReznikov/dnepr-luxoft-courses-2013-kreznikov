SELECT DISTINCT makers.maker_name
FROM product
JOIN makers ON product.maker_id = makers.maker_id
JOIN pc ON pc.model = product.model
WHERE speed >= 450