SELECT AVG(speed) AS 'avg_speed'
FROM ((product a LEFT JOIN PC b ON a.model = b.model) 
	LEFT JOIN makers m ON a.maker_id = m.maker_id
 )
WHERE m.maker_name = 'A';