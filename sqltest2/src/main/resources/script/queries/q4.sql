SELECT DISTINCT makers.maker_name
FROM product
JOIN makers ON product.maker_id = makers.maker_id
WHERE (product.type = 'printer')
ORDER BY makers.maker_name DESC