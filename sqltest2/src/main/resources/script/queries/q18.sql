SELECT a.model
 FROM (((product a LEFT JOIN PC b ON a.model = b.model) 
 LEFT JOIN laptop c ON a.model = c.model )
 LEFT JOIN printer d ON a.model = d.model )
 WHERE COALESCE(b.price, c.price, d.price) IN (SELECT MAX(COALESCE(b.price, c.price, d.price)) FROM
  ((product a LEFT JOIN PC b ON a.model = b.model )
   LEFT JOIN laptop c ON a.model = c.model )
   LEFT JOIN printer d ON a.model = d.model )