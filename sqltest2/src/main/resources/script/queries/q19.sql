SELECT maker_id, AVG(hd) AS 'avg_hd' FROM pc
JOIN product ON product.model = pc.model
WHERE product.maker_id IN (SELECT maker_id FROM product WHERE type = 'printer')
GROUP BY maker_id;