SELECT maker_id, MAX(type) AS 'type'
FROM Product
GROUP BY maker_id
HAVING count(model) > 1 AND MAX(type) = MIN(type);