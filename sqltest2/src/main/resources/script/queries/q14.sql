SELECT maker_name, price
FROM printer
JOIN product ON printer.model = product.model
JOIN makers ON makers.maker_id = product.maker_id
WHERE price IN (SELECT MIN(price) FROM printer WHERE color = 'y')
AND color = 'y';