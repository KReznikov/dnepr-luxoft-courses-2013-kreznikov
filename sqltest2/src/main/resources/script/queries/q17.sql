SELECT DISTINCT maker_id FROM product
WHERE type = 'printer' AND maker_id IN(SELECT maker_id FROM product
       WHERE model IN(SELECT model FROM PC
              WHERE speed = (SELECT MAX(speed) FROM (SELECT speed  FROM PC 
                         WHERE ram = (SELECT MIN(ram) FROM PC )) AS alias)));