SELECT COUNT(*) AS mod_count, maker_id
FROM product
WHERE type='PC'
GROUP BY maker_id 
HAVING COUNT(*) >= 3;