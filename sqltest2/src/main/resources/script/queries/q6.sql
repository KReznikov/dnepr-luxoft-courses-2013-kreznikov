SELECT DISTINCT makers.maker_name
FROM product
JOIN makers ON product.maker_id = makers.maker_id
WHERE type = 'pc' AND maker_name NOT IN (
    SELECT makers.maker_name
    FROM product
	JOIN makers ON product.maker_id = makers.maker_id
    WHERE type = 'laptop'
    );