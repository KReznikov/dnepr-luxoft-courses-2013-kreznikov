package com.luxoft.dnepr.courses.compiler;

import org.junit.Assert;
import org.junit.Test;

public class CompilerTest extends AbstractCompilerTest {

    @Test
    public void testSimple() {
        assertCompiled(4, "2+2");
        assertCompiled(1, ".5+.5");
        assertCompiled(5, "2.5+2.5");
        assertCompiled(10, " 2 + 3 + 3 + 2 ");
        assertCompiled(7, " 2 + 3 + 2 ");
        assertCompiled(1, "2 - 1 ");
        assertCompiled(5, "1 + 2 - 1 + 3");
        assertCompiled(6, " 2 * 3 ");
        assertCompiled(4.5, "  9 /2 ");
        assertCompiled(5, " 2+2/2+2");
        assertCompiled(1, " (2+2)/(2+2)");
    }

    @Test
    public void testComplex() {
        assertCompiled(12, "  (2 + 2 ) * 3 ");
        assertCompiled(8.5, "  2.5 + 2 * 3 ");
        assertCompiled(8.5, "  2 *3 + 2.5");
        assertCompiled(25, "  100 / (2 * 10 / 5)");
        assertCompiled(-1, "  3-2+2-2+1-1*3*1");
        assertCompiled(12, "(((2 + 2 )) * 3)");
    }

    @Test
    public void testHardcore() {
        assertCompiled(29, "  5 + 2 * (14 - 3 * (8 - 6)) + 32 / (10 - 2*3)");
        assertCompiled(5, "((((1+1)+1)+1)+1)");
        assertCompiled(24, "  2 *5/2.5 + (15+5)");
        assertCompiled(4, " 2*2/2*2/2*2/2*2");
        assertCompiled(1, "  100 / 2 / 5 / 10");
    }

    @Test
    public void testErrors() {
        try {
            assertCompiled(1, "  100a / 2 / 5 / 10");
        } catch (CompilationException e) {
            Assert.assertEquals("" + e, "com.luxoft.dnepr.courses.compiler.CompilationException: compilation error");
        }

        try {
            assertCompiled(4, "   2 + + + 2");
        } catch (CompilationException e) {
            Assert.assertEquals("" + e, "com.luxoft.dnepr.courses.compiler.CompilationException: compilation error");
        }

        try {
            assertCompiled(4, "(2 + 2 ");
        } catch (CompilationException e) {
            Assert.assertEquals("" + e, "com.luxoft.dnepr.courses.compiler.CompilationException: compilation error");
        }

        try {
            assertCompiled(2.5, " 5* 0.(  )  5");
        } catch (CompilationException e) {
            Assert.assertEquals("" + e, "com.luxoft.dnepr.courses.compiler.CompilationException: compilation error");
        }

        try {
            assertCompiled(25, " 5+ * 5");
        } catch (CompilationException e) {
            Assert.assertEquals("" + e, "com.luxoft.dnepr.courses.compiler.CompilationException: compilation error");
        }

        try {
            assertCompiled(2.5, null);
        } catch (CompilationException e) {
            Assert.assertEquals("" + e, "com.luxoft.dnepr.courses.compiler.CompilationException: compilation error");
        }
    }

}
