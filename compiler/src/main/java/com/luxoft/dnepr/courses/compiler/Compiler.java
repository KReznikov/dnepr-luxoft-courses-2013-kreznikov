package com.luxoft.dnepr.courses.compiler;

import java.io.ByteArrayOutputStream;
import java.util.*;

public class Compiler {

    public static void main(String[] args) {
        byte[] byteCode = compile(getInputString(args));
        VirtualMachine vm = VirtualMachineEmulator.create(byteCode, System.out);
        vm.run();
    }

    static byte[] compile(String input) {
        ByteArrayOutputStream result = new ByteArrayOutputStream();
        Stack<String> stack = new Stack<>();
        stack.push("PRINT");
        if (input == null) {
            throw new CompilationException("compilation error");
        }
        try {
            parse(input, stack);
            toOutputStream(stack, result);
        } catch (NumberFormatException | IndexOutOfBoundsException e) {
            throw new CompilationException("compilation error");
        }
        return result.toByteArray();
    }

    private static void toOutputStream(Stack<String> stack, ByteArrayOutputStream result) throws NumberFormatException {
        while (!stack.empty()) {
            String command = stack.pop();
            switch (command) {
                case "PRINT":
                    addCommand(result, VirtualMachine.PRINT);
                    break;
                case "+":
                    addCommand(result, VirtualMachine.ADD);
                    break;
                case "-":
                    addCommand(result, VirtualMachine.SUB);
                    break;
                case "*":
                    addCommand(result, VirtualMachine.MUL);
                    break;
                case "/":
                    addCommand(result, VirtualMachine.DIV);
                    break;
                default:
                    addCommand(result, VirtualMachine.PUSH, Double.valueOf(command));
                    break;
            }
        }
    }

    /**
     * Function parses string to command list
     *
     * @param str   input string
     * @param stack stack to store commands
     */

    private static void parse(String str, Stack<String> stack) throws NumberFormatException {
        str = removeBrackets(str);
        int brackets = 0;
        for (Operation operation : Operation.values()) {
            int i = str.length() - 1;
            char symbol = operation.getSymbol();

            while (str.indexOf(symbol) != -1) {
                char ch = str.charAt(i);
                if (brackets == 0 && ch == symbol) {
                    split(str, stack, i, symbol);
                    return;
                } else {
                    brackets = bracketsCheck(ch, brackets);
                }
                i--;
                if (i == -1) break;
            }

        }
        stack.push(str);
    }

    private static int bracketsCheck(char ch, int brackets) {
        if (ch == '(') {
            brackets++;
        } else if (ch == ')') {
            brackets--;
        }
        return brackets;
    }

    private static void split(String str, Stack<String> stack, int i, char symbol) {
        stack.push(String.valueOf(symbol));
        parse(str.substring(0, i), stack);
        parse(str.substring(i + 1), stack);
    }

    /**
     * Function looks for unnecessary brackets and then removes them from string
     *
     * @param str input string
     * @return string
     */

    private static String removeBrackets(String str) throws StringIndexOutOfBoundsException  {
        str = str.trim();
        if ('(' == str.charAt(0) && ')' == str.charAt(str.length() - 1)) {
            int bkt = 1;
            for (int i = 1; i < str.length() - 1; i++) {
                bkt = bracketsCheck(str.charAt(i), bkt);
                if (bkt == 0) {
                    return str;
                }
            }
            str = str.substring(1, str.length() - 1);
            return removeBrackets(str);
        }
        return str;
    }


    /**
     * Adds specific command to the byte stream.
     *
     * @param result
     * @param command
     */
    public static void addCommand(ByteArrayOutputStream result, byte command) {
        result.write(command);
    }

    /**
     * Adds specific command with double parameter to the byte stream.
     *
     * @param result
     * @param command
     * @param value
     */
    public static void addCommand(ByteArrayOutputStream result, byte command, double value) {
        result.write(command);
        writeDouble(result, value);
    }

    private static void writeDouble(ByteArrayOutputStream result, double val) {
        long bits = Double.doubleToLongBits(val);

        result.write((byte) (bits >>> 56));
        result.write((byte) (bits >>> 48));
        result.write((byte) (bits >>> 40));
        result.write((byte) (bits >>> 32));
        result.write((byte) (bits >>> 24));
        result.write((byte) (bits >>> 16));
        result.write((byte) (bits >>> 8));
        result.write((byte) (bits >>> 0));
    }

    private static String getInputString(String[] args) {
        if (args.length > 0) {
            return join(Arrays.asList(args));
        }

        Scanner scanner = new Scanner(System.in);
        List<String> data = new ArrayList<>();
        while (scanner.hasNext()) {
            data.add(scanner.next());
        }
        return join(data);
    }

    private static String join(List<String> list) {
        StringBuilder result = new StringBuilder();
        for (String element : list) {
            result.append(element);
        }
        return result.toString();
    }

}
