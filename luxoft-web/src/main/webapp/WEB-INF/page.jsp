<!DOCTYPE html>
<html>

<head>

    <title>Welcome</title>
    <meta charset="utf-8">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/styles/welcome.css" />
</head>

<body>
    <div id="main">

        <form action="user" method="post">
            <button id="ref" name="logout" value="logout">logout</button>
        <form>

        <h1 id="greetings">Hello <%= session.getAttribute("name") %>!</h1>
    </div>
</body>

</html>