package com.luxoft.dnepr.courses.myFirstWebApp;

public class Person {

    private Integer age;
    private String name;

    public Integer getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Person [age=" + age + ", name=" + name + "]";
    }
}
