package com.luxoft.dnepr.courses.myFirstWebApp;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


public class LoginServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html; charset=utf-8");
        String name = request.getParameter("user");
        String pass = request.getParameter("pass");
        if (name == null) {
            request.getSession().invalidate();
            response.sendRedirect("index.html");
        } else {
            if (isValidUser(name, pass)) {
                login(request, response, name);
            } else {
                request.setAttribute("error", "Wrong login or password");
                request.getRequestDispatcher("index.jsp").forward(request, response);
            }
        }

    }

    private void login(HttpServletRequest request, HttpServletResponse response, String name)
            throws ServletException, IOException {
        request.getSession().setAttribute("name", name);
        request.getRequestDispatcher("WEB-INF/page.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        if (session != null) {
            request.getRequestDispatcher("WEB-INF/page.jsp").forward(request, response);
        } else {
            response.sendRedirect("index.html");
        }
    }


    private boolean isValidUser(String name, String pass) {
        if (!UsersStorage.getData().containsKey(name)) {
            return false;
        }
        return UsersStorage.getData().get(name).equals(pass);
    }

}
