package com.luxoft.dnepr.courses.myFirstWebApp;

import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import com.google.gson.Gson;


public class DummyServlet extends HttpServlet {

    private static ConcurrentMap<String, Integer> storage = new ConcurrentHashMap<>();
    private Gson gson = new Gson();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String json = convertToString(request.getInputStream());
        Person data = gson.fromJson(json, Person.class);

        String name = data.getName();
        Integer age = data.getAge();

        PrintWriter writer = response.getWriter();
        response.setContentType("application/json; charset=utf-8");

        if (isNull(name, age)) {
            illegalParametersResponse(response, writer);
        } else {
            Integer putResult = storage.putIfAbsent(name, age);
            if (putResult == null) {
                response.setStatus(HttpServletResponse.SC_CREATED);
            } else {
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                writer.println("{\"error\": \"Name " + name + " already exists\"}");
            }
        }
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws
            ServletException, IOException {
        String json = convertToString(request.getInputStream());
        Person data = gson.fromJson(json, Person.class);

        String name = data.getName();
        Integer age = data.getAge();

        PrintWriter writer = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("utf-8");

        if (isNull(name, age)) {
            illegalParametersResponse(response, writer);
        } else {
            Integer replaceResult = storage.replace(name, age);
            if (replaceResult == null) {
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                writer.println("{\"error\": \"Name " + name + " does not exist\"}");
            } else {
                response.setStatus(HttpServletResponse.SC_ACCEPTED);
            }
        }
    }

    private static void illegalParametersResponse(HttpServletResponse response, PrintWriter writer) {
        response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        writer.println("{\"error\": \"Illegal parameters\"}");
    }

    private static boolean isNull(String name, Integer age) {
        return name == null || age == null;
    }

    private static String convertToString(ServletInputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader in = new BufferedReader(new InputStreamReader(is));

        String line;
        while ((line = in.readLine()) != null) {
            sb.append(line);
        }
        return sb.toString();
    }


}
