package com.luxoft.dnepr.courses.finalTask;

import com.luxoft.dnepr.courses.finalTask.dao.GenericDao;
import com.luxoft.dnepr.courses.finalTask.dao.IDao;
import com.luxoft.dnepr.courses.finalTask.exceptions.UserAlreadyExist;
import com.luxoft.dnepr.courses.finalTask.model.Employee;
import com.luxoft.dnepr.courses.finalTask.model.Entity;
import com.luxoft.dnepr.courses.finalTask.model.Redis;
import org.junit.Assert;
import org.junit.Test;


public class NewTest {

    @Test
    public void partOneTest() {
        IDao<Entity> genericDao = GenericDao.getInstance();
        Employee man = new Employee(1L, 5200);
        Employee anotherMan = new Employee(2L, 4200);

        // try-catch blocks here, if you will try to re-run tests.
        try {
            genericDao.save(man);
            Assert.assertEquals(man, genericDao.get(1L, "Employee"));
        }catch (UserAlreadyExist e) {
            System.out.print(e.getMessage());
        }

        // try-catch blocks here, if you will try to re-run tests.
        try {
            genericDao.save(anotherMan);
            Assert.assertEquals(anotherMan, genericDao.get(2L, "Employee"));
        }catch (UserAlreadyExist e) {
            System.out.print(e.getMessage());
        }


        man.setSalary(3000);
        genericDao.update(man);
        Assert.assertEquals(man, genericDao.get(1L, "Employee"));

        Assert.assertTrue(genericDao.delete(1L, "Employee"));
    }

    @Test
    public void partTwoTest() {
        IDao<Entity> genericDao = GenericDao.getInstance();

        Redis redis = new Redis(1L, 100);
        Redis anotherRedis = new Redis(2L, 200);

        // try-catch blocks here, if you will try to re-run tests.
        try {
            genericDao.save(redis);
            Assert.assertEquals(redis, genericDao.get(1L, "Redis"));
        }catch (UserAlreadyExist e) {
            System.out.print(e.getMessage());
        }

        // try-catch blocks here, if you will try to re-run tests.
        try {
            genericDao.save(anotherRedis);
            Assert.assertEquals(anotherRedis, genericDao.get(2L, "Redis"));
        }catch (UserAlreadyExist e) {
            System.out.print(e.getMessage());
        }

        redis.setWeight(500);
        genericDao.update(redis);
        Assert.assertEquals(redis, genericDao.get(1L, "Redis"));
    }

}

