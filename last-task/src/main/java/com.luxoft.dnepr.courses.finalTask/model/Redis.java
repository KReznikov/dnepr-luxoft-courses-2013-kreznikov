package com.luxoft.dnepr.courses.finalTask.model;


public class Redis extends Entity {
    private int weight;

    public Redis(long id, int weight) {
        super(id);
        this.weight = weight;
    }

    public Redis() {
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Redis)) return false;
        if (!super.equals(o)) return false;

        Redis redis = (Redis) o;

        if (weight != redis.weight) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + weight;
        return result;
    }
}
