package com.luxoft.dnepr.courses.finalTask.model;


public class Employee extends Entity {
    private int salary;

    public Employee (long id, int salary) {
        super (id);
        this.salary = salary;
    }

    public Employee () {
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Employee)) return false;
        if (!super.equals(o)) return false;

        Employee employee = (Employee) o;

        if (salary != employee.salary) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + salary;
        return result;
    }
}
