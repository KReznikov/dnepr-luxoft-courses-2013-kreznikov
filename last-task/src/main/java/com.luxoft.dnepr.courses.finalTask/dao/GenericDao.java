package com.luxoft.dnepr.courses.finalTask.dao;


import com.luxoft.dnepr.courses.finalTask.ThreeValues;
import com.luxoft.dnepr.courses.finalTask.exceptions.UserAlreadyExist;
import com.luxoft.dnepr.courses.finalTask.exceptions.UserNotFound;
import com.luxoft.dnepr.courses.finalTask.model.Employee;
import com.luxoft.dnepr.courses.finalTask.model.Entity;
import com.luxoft.dnepr.courses.finalTask.model.Redis;
import org.h2.jdbcx.JdbcConnectionPool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class GenericDao<E extends Entity> implements IDao<E> {

    public static final int DUPLICATE_KEY_ERROR = 23505;


    private static GenericDao instance;
    private JdbcConnectionPool connectionPool = JdbcConnectionPool.create("jdbc:h2:~/test", "sa", "");

    private GenericDao() {
    }

    public static synchronized GenericDao getInstance() {
        if (instance == null) {
            instance = new GenericDao();
        }
        return instance;
    }


    public void save(E e) {
        try (Connection connection = getConnection()) {

            doSaveStatement(connection, e.getId(), getDataToUpdate(e));

        } catch (SQLException ex) {

            if (ex.getErrorCode() == DUPLICATE_KEY_ERROR) {
                throw new UserAlreadyExist("this id is already in use");
            } else {
                ex.printStackTrace();
            }

        }
    }

    public void update(E e) {

        Long id = e.getId();

        if (id == null) {
            throw new UserNotFound("there is no this id in storage");
        }

        try (Connection connection = getConnection()) {

            doUpdateStatement(connection, getDataToUpdate(e), id);

        } catch (SQLException ex) {

            ex.printStackTrace();

        }
    }


    public E get(long id, String table) {

        try (Connection connection = getConnection()) {

            return doGetStatement(connection, id, table);

        } catch (SQLException ex) {

            ex.printStackTrace();
            return null;

        }
    }

    public boolean delete(long id, String table) {

        try (Connection connection = getConnection()) {

            return doDeleteStatement(connection, id, table);

        } catch (SQLException e) {

            e.printStackTrace();
            return false;

        }

    }


    private void doUpdateStatement(Connection connection, ThreeValues data, long id) throws SQLException {
        PreparedStatement preparedStatement =
                connection.prepareStatement("UPDATE " + data.getFirst() + " SET " + data.getSecond() + " = ? WHERE id = ?");
        preparedStatement.setInt(2, (int) (long) id);
        preparedStatement.setInt(1, data.getThird());

        if (preparedStatement.executeUpdate() == 0) {
            throw new UserNotFound("there is no this id in storage");
        }
    }

    private E doGetStatement(Connection connection, long id, String table) throws SQLException {

        PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM " + table + " WHERE id = ?");
        preparedStatement.setInt(1, (int) id);
        ResultSet result = preparedStatement.executeQuery();

        if (result.next()) {
            if (table.equals("Redis")) {
                return (E) new Redis(id, result.getInt("weight"));
            } else {
                return (E) new Employee(id, result.getInt("salary"));
            }
        }

        throw new UserNotFound("there is no this id in storage");
    }

    private boolean doSaveStatement(Connection connection, Long id, ThreeValues data) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement("INSERT " +
                "INTO " + data.getFirst() + " (id, " + data.getSecond() + ") VALUES (?,?)");
        preparedStatement.setInt(2, data.getThird());
        if (id != null) {
            preparedStatement.setInt(1, (int) (long) id);
        } else {
            preparedStatement.setString(1, null);
        }

        return preparedStatement.execute();
    }


    private boolean doDeleteStatement(Connection connection, long id, String table) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM " + table + " WHERE id = ?");
        preparedStatement.setInt(1, (int) id);
        return preparedStatement.executeUpdate() != 0;
    }

    private Connection getConnection() {
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }


    private ThreeValues getDataToUpdate(E e) {
        if (e instanceof Employee) {
            return new ThreeValues("EMPLOYEE", "salary", ((Employee) e).getSalary());
        } else {
            return new ThreeValues("REDIS", "weight", ((Redis) e).getWeight());
        }
    }


}
