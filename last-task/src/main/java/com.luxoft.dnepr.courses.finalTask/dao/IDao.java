package com.luxoft.dnepr.courses.finalTask.dao;


import com.luxoft.dnepr.courses.finalTask.model.Entity;

public interface IDao<E extends Entity> {
    void save (E e);
    void update (E e);
    E get (long id, String table);
    boolean delete (long id, String table);
}
