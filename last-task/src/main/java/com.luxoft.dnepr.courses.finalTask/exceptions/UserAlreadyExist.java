package com.luxoft.dnepr.courses.finalTask.exceptions;


public class UserAlreadyExist extends RuntimeException {

    public UserAlreadyExist(String message) {
        super(message);
    }
}
