package com.luxoft.dnepr.courses.finalTask;


public class ThreeValues {

    private String firstValue;
    private String secondValue;
    private int thirdValue;

    public ThreeValues(String firstValue, String secondValue, int thirdValue) {
        this.firstValue = firstValue;
        this.secondValue = secondValue;
        this.thirdValue = thirdValue;
    }

    public String getFirst() {
        return firstValue;
    }

    public String getSecond() {
        return secondValue;
    }

    public int getThird() {
        return thirdValue;
    }

}
