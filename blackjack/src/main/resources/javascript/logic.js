var isGameOver = false;
var win = 0;
var loose = 0;
var push = 0;

function hit() {
    if (isGameOver === false) {
        $.ajax({

            url: "http://localhost:8080/service",
            data: {
                method: 'requestMore'
            }

        }).done(function (response) {
            var root = $.parseJSON(response);
            showNewCards(root.myHand, "#player_card_place", root.total, '#player_text h3');

            if (root.result === false) {
                gameOver("LOOSE");
            }
        });
    }
}

function stand() {
    if (isGameOver == false) {
        $.ajax({

            url: "http://localhost:8080/service",
            data: {
                method: 'stand'
            }

        }).done(function (response) {
            var root = $.parseJSON(response);
            showNewCards(root.dealersHand, "#dealer_card_place", root.total, '#dealer_text h3');
            gameOver(root.winstate)
        });
    }
}

function firstRequest() {
    $.ajax({
        url: "http://localhost:8080/service",
        data: {
            method: 'newGame'
        }

    }).done(function (response) {
        var root = $.parseJSON(response);
        showNewCards(root.myHand, "#player_card_place", root.myTotal, '#player_text h3');
        showNewCards(root.dealersHand, "#dealer_card_place", root.dealersTotal, '#dealer_text h3');
    });
}

function newGame() {
    $('#gameOver').remove();
    clearTable();
    isGameOver = false;
    dealerTotal = 0;
    playerTotal = 0;
    firstRequest();
}

function gameOver(winState) {
    isGameOver = true;
    if (winState === "LOOSE") {
        showPopUp("YOU LOOSE");
        loose++;
        $('#loose').text('LOOSE: ' + loose);
    } else if (winState === "WIN") {
        showPopUp("YOU WIN");
        win++;
        $('#win').text('WIN: ' + win);
    } else {
        showPopUp("PUSH");
        push++;
        $('#push').text('PUSH: ' + push);
    }
}

function showPopUp(message) {
     var tinyWindow = $('<div>');
     tinyWindow.attr('id', 'gameOver');
     addCSSProp(tinyWindow);
     tinyWindow.appendTo($("#main"));
     addHeader(message);
     $(tinyWindow).animate({ opacity: 1 }, 700, addNewGameBtn);
     $(tinyWindow).draggable({ opacity: 0.7 });
}

function addCSSProp(tinyWindow) {
     tinyWindow.css({
        'position': 'absolute',
        'width': '200px',
        'height': '100px',
        'margin': '-300px 278px',
        'background-color': '#ADD8E6',
        'border-radius': '10px',
        'padding': '0px 50px 20px 50px',
        'border': '2px',
        'border-style': 'solid',
        'opacity': '0'
        })
}

function addNewGameBtn() {
    var btn = $('<input type="button" class="button" value="play again" onclick="newGame()" />');
    btn.css({
        'margin': '-3px 30px'
    })
    btn.appendTo($('#gameOver'));
}

function addHeader(message) {
    var header = $('<h1>' + message + '</h1>');
    header.css({
        'text-align': 'center'
    })
    header.appendTo($('#gameOver'));
}

function split() {
    alert("this feature will be available in next updates!");
}

function showNewCards(cards, place, score, person) {
    for (var i = 0; i < cards.length; i++) {
        var img = $('<img>');
        img.attr('src', 'img/' + cards[i].rank + cards[i].suit + '.png');
        img.css('float', 'left');
        img.appendTo($(place));
    }
    $(person).text('total: ' + score);
}

function clearTable() {
    $('#dealer_card_place').empty();
    $('#player_card_place').empty();
}

$(document).ready(firstRequest);