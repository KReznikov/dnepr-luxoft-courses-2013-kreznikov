package com.luxoft.dnepr.courses.unit3.controller;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.Rank;
import com.luxoft.dnepr.courses.unit3.model.Suit;


public final class Deck {

    public static List<Card> createDeck(int size) {
        List<Card> deck = new LinkedList<>();

        if (size > 10) {
            size = 10;
        } else if (size < 1) {
            size = 1;
        }

        for (int i = 0; i < size; i++) {
            for (Suit suit : Suit.values()) {
                for (Rank rank : Rank.values()) {
                    deck.add(new Card(rank, suit));
                }
            }
        }
        return deck;
    }


    public static int costOf(List<Card> hand) {
        int cost = 0;
        for (Card card : hand) {
            cost = cost + card.getCost();
        }
        return cost;
    }
}
