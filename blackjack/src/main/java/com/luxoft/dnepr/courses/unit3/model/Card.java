package com.luxoft.dnepr.courses.unit3.model;

public class Card {

    Rank rank;
    Suit suit;

	public Card(Rank rank, Suit suit) {
        this.rank = rank;
        this.suit = suit;
	}

	public Rank getRank() {
		return rank;
	}
	
	public int getCost() {
		return rank.getCost();
	}

    public Suit getSuit() {
        return suit;
    }

}
