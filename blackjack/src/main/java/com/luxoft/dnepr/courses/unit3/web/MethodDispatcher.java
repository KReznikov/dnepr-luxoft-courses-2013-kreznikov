package com.luxoft.dnepr.courses.unit3.web;

import java.util.List;

import com.luxoft.dnepr.courses.unit3.controller.Deck;
import com.luxoft.dnepr.courses.unit3.controller.GameController;
import com.luxoft.dnepr.courses.unit3.model.Card;

public class MethodDispatcher {

    /**
     * @param request
     * @param response
     * @return response or <code>null</code> if wasn't able to find a method.
     */
    public Response dispatch(Request request, Response response) {
        String method = request.getParameters().get("method");
        if (method == null) {
            return null;
        }
        if (method.equals("requestMore")) {
            return requestMore(response);
        } else if (method.equals("newGame")) {
            GameController.getInstance().newGame();
            return gameState(response);
        } else if (method.equals("stand")) {
            GameController.getInstance().requestStop();
            return requestStand(response);
        }

        return null;
    }

    private Response requestMore(Response response) {
        response.write("{\"result\": " + GameController.getInstance().requestMore());
        response.write(", \"myHand\": ");

        List hand = GameController.getInstance().getMyHand();
        writeHand(response, hand.subList(hand.size() - 1, hand.size()));
        response.write(", \"total\": " + Deck.costOf(hand));
        response.write("}");

        return response;
    }


    private Response requestStand(Response response) {
        response.write("{\"winstate\": \"" + GameController.getInstance().getWinState());
        response.write("\", \"dealersHand\": ");

        List hand = GameController.getInstance().getDealersHand();
        writeHand(response, hand.subList(1, hand.size()));
        response.write(", \"total\": " + Deck.costOf(hand));
        response.write("}");

        return response;
    }



    private Response gameState(Response response) {
        List myHand = GameController.getInstance().getMyHand();
        List dealersHand = GameController.getInstance().getDealersHand();
        response.write("{\"myHand\": ");
        writeHand(response, myHand);
        response.write(", \"dealersHand\": ");
        writeHand(response, dealersHand);
        response.write(", \"myTotal\": " + Deck.costOf(myHand));
        response.write(", \"dealersTotal\": " + Deck.costOf(dealersHand));
        response.write("}");

        return response;
    }


    private void writeHand(Response response, List<Card> hand) {
        boolean isFirst = true;
        response.write("[");
        for (Card card : hand) {
            if (isFirst) {
                isFirst = false;
            } else {
                response.write(",");
            }
            response.write("{\"rank\": \"");
            response.write(card.getRank().getName());
            response.write("\", \"suit\": \"");
            response.write(card.getSuit().name());
            response.write("\"}");
        }
        response.write("]");
    }

}
