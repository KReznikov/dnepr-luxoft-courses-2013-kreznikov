package com.luxoft.dnepr.courses.unit3.view;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.List;
import java.util.Scanner;

import com.luxoft.dnepr.courses.unit3.controller.Deck;
import com.luxoft.dnepr.courses.unit3.controller.GameController;
import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.WinState;

public class CommandLineInterface {

    private Scanner scanner;
    private PrintStream output;

    public CommandLineInterface(PrintStream output, InputStream input) {
        this.scanner = new Scanner(input);
        this.output = output;
    }

    public void play() {
        printAbout();
        GameController controller = GameController.getInstance();
        controller.newGame();

        output.println();
        printState(controller);

        while (scanner.hasNext()) {
            String command = scanner.next();
            if (!execute(command, controller)) {
                return;
            }
        }
    }

    private void printAbout() {
        output.println("Console Blackjack application.\n" +
                "Author: Kirill Reznikov\n" +
                "(C) Luxoft 2013\n");
    }

    /**
     * Выполняем команду, переданную с консоли. Список разрешенных комманд можно найти в классе {@link Command}.
     * Используйте методы контроллера чтобы обращаться к логике игры. Этот класс должен содержать только интерфейс.
     * Если этот метод вернет false - игра завершится.
     * <p/>
     * Более детальное описание формата печати можно узнать посмотрев код юниттестов.
     *
     *      <p/>
     *      Описание команд:
     *      Command.HELP - печатает помощь.
     *      Command.MORE - еще одну карту и напечатать Состояние (GameController.requestMore())
     *      если после карты игрок проиграл - напечатать финальное сообщение и выйти
     *      Command.STOP - игрок закончил, теперь играет диллер (GameController.requestStop())
     */
    private boolean execute(String command, GameController controller) {

        switch (command) {
            case Command.HELP:
                printHelp();
                break;
            case Command.MORE:
                moreCards(controller);
                break;
            case Command.STOP:
                stand(controller);
                break;
            case Command.EXIT:
                output.println("bye");
                return false;
            default:
                output.println("Invalid command");
                break;
        }
        return true;
    }

    private void printHelp() {
        output.println("Usage: \n" +
                "\thelp - prints this message\n" +
                "\thit - requests one more card\n" +
                "\tstand - I'm done - lets finish\n" +
                "\texit - exits game");
    }

    private void moreCards(GameController controller) {
        controller.requestMore();
        printState(controller);
        if (Deck.costOf(controller.getMyHand()) > GameController.MAX_POINTS) {
            printGameOver(controller.getWinState());
        }
    }

    private void stand (GameController controller) {
        controller.requestStop();
        output.println("Dealer turn:\n");
        printState(controller);
        printGameOver(controller.getWinState());
    }

    private void printGameOver(WinState state) {
        switch (state) {
            case WIN:
                output.println("\nCongrats! You win!");
                break;
            case LOOSE:
                output.println("\nSorry, today is not your day. You loose.");
                break;
            default:
                output.println("\nPush. Everybody has equal amount of points.");
                break;
        }
    }

    private void printState(GameController controller) {
        List<Card> myHand = controller.getMyHand();
        format(myHand);
        List<Card> dealersHand = controller.getDealersHand();
        format(dealersHand);
    }


    private void format(List<Card> hand) {
        for (Card card : hand) {
            output.print(card.getRank().getName() + " ");
        }
        output.println("(total " + Deck.costOf(hand) + ")");
    }
}