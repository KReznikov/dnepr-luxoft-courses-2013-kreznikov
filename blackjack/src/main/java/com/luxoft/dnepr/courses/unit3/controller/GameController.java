package com.luxoft.dnepr.courses.unit3.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.WinState;

public class GameController {
    private static GameController controller;
    public List<Card> playerHand;
    public List<Card> dealerHand;
    public List<Card> deck;
    public static final int MAX_POINTS = 21;

    private GameController() {
        this.playerHand = new ArrayList<>();
        this.dealerHand = new ArrayList<>();
    }

    public static GameController getInstance() {
        if (controller == null) {
            controller = new GameController();
        }

        return controller;
    }

    public void newGame() {
        newGame(new Shuffler() {
            @Override
            public void shuffle(List<Card> deck) {
                Collections.shuffle(deck);
            }
        });
    }

    /**
     * Создает новую игру.
     * - перемешивает колоду (используйте для этого shuffler.shuffle(list))
     * - раздает две карты игроку
     * - раздает одну карту диллеру.
     *
     * @param shuffler shuffle the deck
     */
    void newGame(Shuffler shuffler) {
        this.playerHand.clear();
        this.dealerHand.clear();
        this.deck = Deck.createDeck(1);
        shuffler.shuffle(deck);
        playerHand.add(takeCard(deck));
        playerHand.add(takeCard(deck));
        dealerHand.add(takeCard(deck));
    }

    private Card takeCard(List<Card> deck) {
        return deck.remove(0);
    }

    /**
     * Метод вызывается когда игрок запрашивает новую карту.
     * - если сумма очков на руках у игрока больше максимума или колода пуста - ничего не делаем
     * - если сумма очков меньше - раздаем игроку одну карту из коллоды.
     *
     * @return true если сумма очков у игрока меньше максимума (или равна) после всех операций и false если больше.
     */
    public boolean requestMore() {
        if (!deck.isEmpty() && isLessThenMax(playerHand) ) {
            playerHand.add(takeCard(deck));
        }
        return isLessThenMax (playerHand);
    }

    private boolean isLessThenMax (List<Card> hand) {
        return (Deck.costOf(hand) <= MAX_POINTS);
    }

    /**
     * Вызывается когда игрок получил все карты и хочет чтобы играло казино (диллер).
     * Сдаем диллеру карты пока у диллера не наберется 17 очков.
     */
    public void requestStop() {
        while (Deck.costOf(dealerHand) < 17 ) {
            dealerHand.add(takeCard(deck));
        }
    }

    /**
     * Сравниваем руку диллера и руку игрока.
     * Если у игрока больше максимума - возвращаем WinState.LOOSE (игрок проиграл)
     * Если у игрока меньше чем у диллера и у диллера не перебор - возвращаем WinState.LOOSE (игрок проиграл)
     * Если очки равны - это пуш (WinState.PUSH)
     * Если у игрока больше чем у диллера и не перебор - это WinState.WIN (игрок выиграл).
     */
    public WinState getWinState() {
        if (!isLessThenMax(playerHand)) {
            return WinState.LOOSE;
        }
        else if (Deck.costOf(playerHand) < Deck.costOf(dealerHand) && isLessThenMax(dealerHand)) {
            return WinState.LOOSE;
        }
        else if (Deck.costOf(playerHand) == Deck.costOf(dealerHand)) {
            return WinState.PUSH;
        }
        else {
            return WinState.WIN;
        }
    }

    /**
     * Возвращаем руку игрока
     */
    public List<Card> getMyHand() {
        return playerHand;
    }

    /**
     * Возвращаем руку диллера
     */
    public List<Card> getDealersHand() {
        return dealerHand;
    }
}
