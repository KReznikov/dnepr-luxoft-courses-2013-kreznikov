package com.luxoft.dnepr.courses.mySecondWebApp;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;


public class UsersStorage implements ServletContextListener {
    ServletContext context;
    private static Map<String, UserData> data = new HashMap<>();

    public void contextInitialized(ServletContextEvent contextEvent) {
        context = contextEvent.getServletContext();
        String userPath = context.getRealPath("/META-INF/" + context.getInitParameter("users"));
        deserializeFromXML(userPath);
        initAttributes();

    }

    private void initAttributes () {
        context.setAttribute("ACTIVE_SESSIONS", new AtomicLong(0));
        context.setAttribute("ADMIN_SESSIONS", new AtomicLong(0));
        context.setAttribute("USER_SESSIONS", new AtomicLong(0));
        context.setAttribute("TOTAL_HTTP_REQUESTS", new AtomicLong(0));
        context.setAttribute("HTTP_POST_REQUESTS", new AtomicLong(0));
        context.setAttribute("HTTP_GET_REQUESTS", new AtomicLong(0));
        context.setAttribute("HTTP_OTHER_REQUESTS", new AtomicLong(0));
    }

    public void contextDestroyed(ServletContextEvent contextEvent) {
        context = contextEvent.getServletContext();
    }

    public static Map<String, UserData> getData() {
        return Collections.unmodifiableMap(data);
    }

    public static String getUserRole(String name) {
        if (data.containsKey(name)){
            return data.get(name).getRole();
        }
        return null;
    }

    public static String getUserPass(String name) {
        if (data.containsKey(name)){
            return data.get(name).getPassword();
        }
        return null;
    }

    private static void addEntry(NamedNodeMap attrs) {
        String role = attrs.item(2).getNodeValue();
        String password = attrs.item(1).getNodeValue();
        String name = attrs.item(0).getNodeValue();
        data.put(name, new UserData(password, role));
    }

    private static void deserializeFromXML(String absolutePathToFile) {
        try {
            File file = new File(absolutePathToFile);
            DocumentBuilder dBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            NodeList nodeList = doc.getDocumentElement().getChildNodes();
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    addEntry(node.getAttributes());
                }
            }
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
    }


}
