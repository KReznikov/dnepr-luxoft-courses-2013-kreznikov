package com.luxoft.dnepr.courses.mySecondWebApp.Listeners;

import javax.servlet.ServletContext;
import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.atomic.AtomicLong;


public class RequestsListenerAndCounter implements ServletRequestListener {

    @Override
    public void requestDestroyed(ServletRequestEvent sre) {
    }

    @Override
    public void requestInitialized(ServletRequestEvent sre) {
        ServletContext context = sre.getServletContext();
        HttpServletRequest request = (HttpServletRequest) sre.getServletRequest();
        String HttpMethod = request.getMethod();

        if (HttpMethod.equalsIgnoreCase("GET")) {
            getGetRequests(context).getAndIncrement();
        } else if (HttpMethod.equalsIgnoreCase("POST")) {
            getPostRequests(context).getAndIncrement();
        } else {
            getOtherRequests(context).getAndIncrement();
        }

        getTotalRequests(context).getAndIncrement();
    }


    private AtomicLong getTotalRequests(ServletContext context) {
        return (AtomicLong) context.getAttribute("TOTAL_HTTP_REQUESTS");
    }

    private AtomicLong getPostRequests(ServletContext context) {
        return (AtomicLong) context.getAttribute("HTTP_POST_REQUESTS");
    }

    private AtomicLong getGetRequests(ServletContext context) {
        return (AtomicLong) context.getAttribute("HTTP_GET_REQUESTS");
    }


    private AtomicLong getOtherRequests(ServletContext context) {
        return (AtomicLong) context.getAttribute("HTTP_OTHER_REQUESTS");
    }


}
