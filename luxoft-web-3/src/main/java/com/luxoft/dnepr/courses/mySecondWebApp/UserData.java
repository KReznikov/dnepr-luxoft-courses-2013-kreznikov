package com.luxoft.dnepr.courses.mySecondWebApp;


public class UserData {

    private String password;
    private String role;

    public UserData(String password, String role) {
        this.password = password;
        this.role = role;
    }

    public String getRole() {
        return role;
    }

    public String getPassword() {
        return password;
    }
}
