package com.luxoft.dnepr.courses.mySecondWebApp.Listeners;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.util.concurrent.atomic.AtomicLong;

public class SessionListenerAndCounter implements HttpSessionListener {

    @Override
    public void sessionCreated(HttpSessionEvent hse) {
        getActiveSessions(hse).getAndIncrement();
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent hse) {
        getActiveSessions(hse).getAndDecrement();
        String role = (String) hse.getSession().getAttribute("role");
        if (role.equals("user")) {
            getActiveUserSessions(hse).getAndDecrement();
        } else {
            getActiveAdminSessions(hse).getAndDecrement();
        }
    }

    private AtomicLong getActiveSessions(HttpSessionEvent hse) {
        return (AtomicLong) hse.getSession().getServletContext().getAttribute("ACTIVE_SESSIONS");
    }

    private AtomicLong getActiveUserSessions(HttpSessionEvent hse) {
        return (AtomicLong) hse.getSession().getServletContext().getAttribute("USER_SESSIONS");
    }

    private AtomicLong getActiveAdminSessions(HttpSessionEvent hse) {
        return (AtomicLong) hse.getSession().getServletContext().getAttribute("ADMIN_SESSIONS");
    }


}
