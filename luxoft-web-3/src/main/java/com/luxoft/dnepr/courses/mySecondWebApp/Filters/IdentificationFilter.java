package com.luxoft.dnepr.courses.mySecondWebApp.Filters;

import com.luxoft.dnepr.courses.mySecondWebApp.UsersStorage;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class IdentificationFilter implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        response.setContentType("text/html; charset=utf-8");
        String method = request.getMethod();

        if (method.equalsIgnoreCase("POST")) {
            filterPOST(request, response, filterChain);
        } else if (method.equalsIgnoreCase("GET")) {
            filterGET(request, response, filterChain);
        } else {
            //unsupported method, you will see white page in this case.
        }
    }


    private void filterPOST(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws IOException, ServletException {
        String name = request.getParameter("user");
        String pass = request.getParameter("pass");
        if (name == null) {
            logout(request, response);
        } else {
            if (isValidUser(name, pass)) {
                login(request, response, filterChain, name);
            } else {
                request.setAttribute("error", "Wrong login or password");
                request.getRequestDispatcher("index.html").forward(request, response);
            }
        }
    }

    private void filterGET(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws IOException, ServletException {
        HttpSession session = request.getSession(false);
        if (session == null) {
            response.sendRedirect("index.html");
        } else {
            filterChain.doFilter(request, response);
        }
    }

    private void login(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain, String name)
            throws IOException, ServletException {
        request.getSession().setAttribute("name", name);
        request.getSession().setAttribute("role", UsersStorage.getUserRole(name));
        filterChain.doFilter(request, response);
    }


    private void logout(HttpServletRequest request, HttpServletResponse response) throws IOException {
        request.getSession().invalidate();
        response.sendRedirect("index.html");
    }


    private boolean isValidUser(String name, String pass) {
        if (!UsersStorage.getData().containsKey(name)) {
            return false;
        }
        return UsersStorage.getUserPass(name).equals(pass);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        //unused
    }

    @Override
    public void destroy() {
        //unused
    }

}