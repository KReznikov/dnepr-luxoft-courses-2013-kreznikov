package com.luxoft.dnepr.courses.mySecondWebApp.Listeners;


import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;
import java.util.concurrent.atomic.AtomicLong;

public class AttributeListener implements HttpSessionAttributeListener {



    private AtomicLong getActiveUserSessions(HttpSessionBindingEvent hsbe) {
        return (AtomicLong) hsbe.getSession().getServletContext().getAttribute("USER_SESSIONS");
    }

    private AtomicLong getActiveAdminSessions(HttpSessionBindingEvent hsbe) {
        return (AtomicLong) hsbe.getSession().getServletContext().getAttribute("ADMIN_SESSIONS");
    }

    @Override
    public void attributeAdded(HttpSessionBindingEvent httpSessionBindingEvent) {
        if (httpSessionBindingEvent.getName().equals("role")) {
            String role = (String) httpSessionBindingEvent.getValue();
            if (role.equals("user")) {
                getActiveUserSessions(httpSessionBindingEvent).getAndIncrement();
            } else {
                getActiveAdminSessions(httpSessionBindingEvent).getAndIncrement();
            }
        }
    }

    @Override
    public void attributeRemoved(HttpSessionBindingEvent httpSessionBindingEvent) {
    }

    @Override
    public void attributeReplaced(HttpSessionBindingEvent httpSessionBindingEvent) {
    }
}
