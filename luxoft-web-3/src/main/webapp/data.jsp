<!DOCTYPE html>
<html>
 <head>
  <meta charset="utf-8">
  <title>Statistic</title>
  <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/styles/data-page.css" />
 </head>
 <body>
  <table>
	<tr><th>Parameter</th><th>Value</th></tr>
	<tr><th>Active Sessions</th><th><%= getServletContext().getAttribute("ACTIVE_SESSIONS") %></th></tr>
	<tr><th>Active Sessions (ROLE user)</th><th><%= getServletContext().getAttribute("USER_SESSIONS") %></th></tr>
	<tr><th>Active Sessions (ROLE admin)</th><th><%= getServletContext().getAttribute("ADMIN_SESSIONS") %></th></tr>
	<tr><th>Total Count of HttpRequests</th><th><%= getServletContext().getAttribute("TOTAL_HTTP_REQUESTS") %></th></tr>
	<tr><th>Total Count of POST HttpRequests</th><th><%= getServletContext().getAttribute("HTTP_POST_REQUESTS") %></th></tr>
	<tr><th>Total Count of GET HttpRequests</th><th><%= getServletContext().getAttribute("HTTP_GET_REQUESTS") %></th></tr>
	<tr><th>Total Count of Other HttpRequests</th><th><%= getServletContext().getAttribute("HTTP_OTHER_REQUESTS") %></th></tr>
  </table>
 </body>
</html>
